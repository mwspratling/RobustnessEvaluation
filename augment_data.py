#############################################################################
# Code that defines methods for augmenting images
#
# (c) 2023 Michael William Spratling
#############################################################################

import numpy as np
import torch
import torchattacks
import random
from utils import parse_string #, print_stats

def augment_data(samples, labels, dataAug, model, epsilon, stepsize, steps, noiseStd, noisePatchHalfSize, noiseSparsity, proportion, pixmix_loader):
# calls different data augmentation methods depending on the experimental conditions

    if dataAug.startswith('noise'):
        samples=add_gaussian_noise(samples, noiseStd[0], noiseStd[1], [0.0,1.0])
    if dataAug.startswith('sparsenoise'):
        samples=add_sparse_gaussian_noise(samples, noiseStd[0], noiseStd[1], noiseSparsity, [0.0,1.0])
    if dataAug.startswith('patchnoise'):
        samples=add_gaussian_noise_patch(samples, noiseStd[0], noiseStd[1], noisePatchHalfSize, [0.0,1.0])
    if dataAug.startswith('merge'):
        mergeStrength=dataAug.split('merge')
        mergeStrength=float(mergeStrength[1])
        samples=mix_pixels(samples, mergeStrength, 1, [0.0,1.0])
        #samples=add_gaussian_noise(samples, noiseStd[0], noiseStd[1], [0.0,1.0])
    if dataAug.startswith('FGSM'):
        attack = torchattacks.FGSM(model.eval(), eps=epsilon)
        samples = attack(samples, labels)
        model.train()
        model.training=True
    if dataAug.startswith('PGDLinf'):
        attack = torchattacks.PGD(model.eval(), eps=epsilon, alpha=stepsize, steps=steps, random_start=True)
        samples = attack(samples, labels)
        model.train()
        model.training=True
    if dataAug.startswith('PGDL2'):
        attack = torchattacks.PGDL2(model.eval(), eps=epsilon, alpha=stepsize, steps=steps, random_start=True)
        samples = attack(samples, labels)
        model.train()
        model.training=True
    if dataAug.startswith('mixed'):
        epsilonLinf,epsilonL2=epsilon
        stepsizeLinf,stepsizeL2=stepsize
        select=np.random.uniform()
        if select<np.sum(proportion[:1]):
            samples=add_gaussian_noise(samples, noiseStd[0], noiseStd[1], [0.0,1.0])
        elif select<np.sum(proportion[:2]):
            attack = torchattacks.PGD(model.eval(), eps=epsilonLinf, alpha=stepsizeLinf, steps=steps, random_start=True)
            samples = attack(samples, labels)
        elif select<np.sum(proportion[:3]):
            attack = torchattacks.PGDL2(model.eval(), eps=epsilonL2, alpha=stepsizeL2, steps=steps, random_start=True)
            samples = attack(samples, labels)
        else:
            for samplesMix,_ in pixmix_loader:
                break # get one batch, then break. Loader needs to shuffle data to avoid getting same 1st batch each time
            samples=pixmix(samples,samplesMix)
        model.train()
        model.training=True
    if 'pixmix' in dataAug: #apply randomised mixing with other data
        for samplesMix,_ in pixmix_loader:
            break # get one batch, then break. Loader needs to shuffle data to avoid getting same 1st batch each time
        samples=pixmix(samples,samplesMix)

    return samples


def data_augmentation_hyperparameters(task,dataAug,batchSize,imDims,model):
    #define default parameters for data augmentations that can be used during training
    #note all values are defaults that can be overwritten by values specified within the dataAug string,
    #e.g. dataAug='noise0.2-0.4' would specify noiseStd=[0.2,0.4] replacing the default defined below
    #e.g. dataAug='PGDLinf-20-0.1' would specify 20 step PGD with an epsilon of 0.1 replacing the defaults defined below
    
    #set default values for parameters
    noiseStd=[0.0,0.4]
    if dataAug.startswith('patchnoise'):
        noiseStd=[0.0,0.5]
        noisePatchHalfSize=int(0.375*imDims[-1])
    epsilonLinf=8/255
    epsilonL2=0.5
    steps=10
    noiseSparsity=0.9
    #if 'pixmix' in dataAug:
    _, pixmix_loader, _, _, _, _ = load_dataset('FractalsAndFvis',batchSize,imDimsReqd=list(imDims),shuffle=True)
    noisePatchHalfSize=False
    proportion=False
    mixupAlpha=None
    #set specific defaults for certain datasets
    if task.startswith('MNIST'): 
        noiseStd=[0.5,0.5]
        if dataAug.startswith('patchnoise'):
            noiseStd=[0.0,0.8]
            noisePatchHalfSize=10
        epsilonLinf=0.3
        epsilonL2=2
        
    epsilon=epsilonLinf #default to Linf AT
    stepsize=2.5*epsilon/steps

    #decode the string dataAug that defines the augmentation method to use
    if dataAug.startswith('noise'): #noise specified with format: noiseSTD1-STD2
        augMethod=parse_string(dataAug,'noise',limit='+')
        if len(augMethod[0])==0:
            #use default params
            pass
        elif len(augMethod)==1:
            #if only one value specified assume constant std
            std1=float(augMethod[0])
            noiseStd=[std1,std1]
        if len(augMethod)==2:
            noiseStd=[float(augMethod[0]),float(augMethod[1])]
        print('Data Augmentation: noise with std=',noiseStd)
        
    if dataAug.startswith('sparsenoise'): #noise specified with format: noiseSTD1-STD2-sparsity
        augMethod=parse_string(dataAug,'sparsenoise',limit='+')
        if len(augMethod[0])==0:
            #use default params
            pass
        elif len(augMethod)>=1:
            noiseSparsity=float(augMethod[0])
        if len(augMethod)==2:
            #if only one value specified assume constant std
            std1=float(augMethod[1])
            noiseStd=[std1,std1]
        if len(augMethod)==3:
            noiseStd=[float(augMethod[1]),float(augMethod[2])]
        print('Data Augmentation: sparse noise with std=',noiseStd,'and sparsity=',noiseSparsity)
        
    if dataAug.startswith('patchnoise'): #patchnoise specified with format: patchnoiseHALFSIZE-STD1-STD2
        augMethod=parse_string(dataAug,'patchnoise',limit='+')
        if len(augMethod[0])==0:
            #use default params
            pass
        elif len(augMethod)>=1:
            noisePatchHalfSize=int(augMethod[0])
        if len(augMethod)==2:
            #if only one std value specified assume constant std
            std1=float(augMethod[1])
            noiseStd=[std1,std1]
        if len(augMethod)==3:
            noiseStd=[float(augMethod[1]),float(augMethod[2])]
        print('Data Augmentation: patchnoise with std=',noiseStd,'half-size=',noisePatchHalfSize)
     
    if dataAug.startswith('FGSM'): #FGSM specified with format: FGSMLNORM-EPSILON
        augMethod=parse_string(dataAug,'FGSM',limit='+')
        #if params not defined by expt_def use defaults
        if len(augMethod[0])==0: 
            augMethod=['Linf']
        if len(augMethod)==1:
            augMethod+=[str(epsilonLinf)]
            
        epsilon=float(augMethod[1])
        if augMethod[0]=='Linf':
            attack = torchattacks.FGSM(model.eval(), eps=epsilon)
        elif augMethod[0]=='L2':
            attack = False
        print('Data Augmentation:',attack,'epsilon=',epsilon)
    
    if dataAug.startswith('PGD'): #PGD specified with format: PGDLNORM-NUMSTEPS-EPSILON
        augMethod=parse_string(dataAug,'PGD',limit='+')
        print(augMethod)
        #if params not defined by expt_def use defaults
        if len(augMethod[0])==0: 
            augMethod=['Linf']
        if len(augMethod)==1:
            augMethod+=[str(10)]
        if len(augMethod)==2:
            augMethod+=[str(epsilonLinf)]
            
        steps=int(augMethod[1])
        epsilon=float(augMethod[2])
        stepsize=2.5*epsilon/steps
        if augMethod[0]=='Linf':
            attack = torchattacks.PGD(model.eval(), eps=epsilon, alpha=stepsize, steps=steps, random_start=True)
        elif augMethod[0]=='L2':
            attack = torchattacks.PGDL2(model.eval(), eps=epsilon, alpha=stepsize, steps=steps, random_start=True)
        print('Data Augmentation:',attack,'epsilon=',epsilon)
    
    if dataAug.startswith('mixed'):
        epsilon=[epsilonLinf,epsilonL2]
        stepsize=[2.5*epsilonLinf/steps,2.5*epsilonL2/steps]
        attackLinf = torchattacks.PGD(model.eval(), eps=epsilonLinf, alpha=stepsize[0], steps=steps, random_start=True)
        attackL2 = torchattacks.PGDL2(model.eval(), eps=epsilonL2, alpha=stepsize[1], steps=steps, random_start=True)
        proportion=[0.0,0.5,0.0,0.5]
        proportion=proportion/np.sum(proportion)
        print('Data Augmentation: mixed')
        print('noise with std=',noiseStd)
        print(attackLinf,'epsilon=',epsilonLinf)
        print(attackL2,'epsilon=',epsilonL2)
        print('pixmix')
        print('in proportion',proportion)
    
    if dataAug.startswith('mixup'):
        mixupAlpha=0.2
    if 'regmixup' in dataAug:
        mixupAlpha=10.0
     
    return epsilon, stepsize, steps, noiseStd, noisePatchHalfSize, noiseSparsity, proportion, pixmix_loader, mixupAlpha


def resize_data_to_match_batch(batchSizeReqd,data):
    #increase or decrease number of samples in "data" to match "batchSizeReqd"
    
    batchSize=data.size(0)
    if batchSize<batchSizeReqd:
        #increase amount of data available to have enough to make a new batch of size batchSize
        #by adding copies of data samples to end of current data
        data=data.repeat(int(np.ceil(batchSizeReqd/batchSize)),1,1,1)
        batchSize=data.size(0)
    if batchSize>batchSizeReqd:
        #decrease amount of data to match required batchSize for required new samples
        data=data[0:batchSizeReqd]
    return data


def add_gaussian_noise(data, stdMin, stdMax, clip):
    #Gaussian distributed random values added independently to each element in the data tensor.
    if stdMin!=stdMax:
        stdMax=(stdMax-stdMin)*torch.rand((data.size(0),1,1,1))+stdMin
        stdMax=stdMax.to(data.device)                                                                                         
        
    noise=torch.randn(data.size()).to(data.device)                                                                  
    data = data + stdMax*noise

    #randInd=torch.rand(data.size(0))<0.5
    #data[randInd,:]=dataC[randInd,:]
    
    #clip the result into the range specified
    data = torch.max(torch.tensor(clip[0]),data)
    data = torch.min(torch.tensor(clip[1]),data)
    return data


def add_sparse_gaussian_noise(data, stdMin, stdMax, sparsity, clip):
    #Gaussian distributed random values are sparsified, scaled and added independently to each element in the data tensor.
    if stdMin!=stdMax:
        stdMax=(stdMax-stdMin)*torch.rand((data.size(0),1,1,1))+stdMin
        stdMax=stdMax.to(data.device)                                                                                         
    sparsity*=torch.rand(1).to(data.device)
    
    noise=torch.randn(data.size()).to(data.device)
    #noiseMagOrig=torch.sum(torch.abs(noise), (1,2,3), keepdim=True)
    
    sparseInd=torch.rand(data.size()).to(data.device)<sparsity
    noise[sparseInd]=0.0
    #noiseMag=torch.sum(torch.abs(noise), (1,2,3), keepdim=True)
    #noise*=noiseMagOrig/noiseMag
    data = data + stdMax*noise

    #clip the result into the range specified
    data = torch.max(torch.tensor(clip[0]),data)
    data = torch.min(torch.tensor(clip[1]),data)
    return data


from kornia.morphology import dilation
def add_gaussian_noise_patch(data, stdMin, stdMax, patchHalfSize, clip):
    #add Gaussian distributed random values to a random patch of each image
    dataSize=list(data.size()) 
    #define the standard deviation of the noise to use in each image in the batch
    if stdMin!=stdMax:
        stdMax=(stdMax-stdMin)*torch.rand((dataSize[0],1,1,1))+stdMin
        stdMax=stdMax.to(data.device)                                                                                         

    #define mask for patch: same patch used for all samples in batch for speed
    #choose random location for centre of noise patch
    centreH=random.randrange(0, dataSize[2])
    centreV=random.randrange(0, dataSize[3])
    if centreH<=patchHalfSize and centreV<=patchHalfSize and dataSize[2]-centreH-1<=patchHalfSize and dataSize[3]-centreV-1<=patchHalfSize:
        patch=1 # noise applied to all of the image
    else:
        patchSize=1+2*patchHalfSize #ensure odd size which is required for dilation
        kernel=torch.ones(patchSize,patchSize)
        patch=torch.zeros(1,1,dataSize[2],dataSize[3])
        patch[:,:,centreH,centreV]=1.0
        patch=dilation(patch,kernel).to(data.device)
        
    #apply the noise
    noise=torch.randn(dataSize).to(data.device)                                                                  
    data = data + patch*stdMax*noise
    
    #clip the result into the range specified
    data = torch.max(torch.tensor(clip[0]),data)
    data = torch.min(torch.tensor(clip[1]),data)
    return data

from kornia.geometry.transform import warp_perspective
def warp_perspective_norm(images, warpMatrix):
    # a wrapper for the kornia function "warp_perspective", that normalises the
    # magnitude of warping across different image sizes, and that also produces
    # a mask showing where the warpped image has been padded. Uses nearest-neighbour
    # interpolation rather than biliner interpolation, as the latter causes nan
    # values of loss if gradients are back-propagated through warped feature-maps.
    dataSize=torch.as_tensor(images.size())
    #perform perspective warp that is scaled by the size of the image
    #this ensures same warpMatrix applied to small and large images has same relative effect
    diagLen=torch.sqrt(torch.sum(dataSize[2:]**2))
    I = torch.eye(3)[None].to(images.device)
    images=warp_perspective(images, I+warpMatrix/diagLen, dataSize[2:], mode='nearest')
    #create a mask to distinguish transformed pixels from padding
    mask=torch.ones((dataSize[0],1,dataSize[2],dataSize[3])).to(images.device)
    mask=warp_perspective(mask, I+warpMatrix/diagLen, dataSize[2:], mode='nearest')
    return images, mask

def random_perspective(data, warpMag=0.1):
    dataSize=torch.as_tensor(data.size())
    warpMatrix=warpMag*torch.randn((dataSize[0],3,3)).to(data.device)
    data,_=warp_perspective_norm(data, warpMatrix)
    return data, warpMatrix

def mix_pixels(data, mergeStrengthMax, repeats, clip):
    #Make pixel values a weighted average (where the weight is a random value) of the original values and those taken from other images in the batch
    dataSize=torch.as_tensor(data.size()).numpy()
    
    for it in range(repeats):

        # merge with pixels from one other image
        otherImage=data[torch.randperm(data.size(0)),:]
        
        # merge with random pixel from random image
        #numPixels=int(dataSize.prod()/dataSize[1])
        #otherImage=data.permute((1,0,2,3)).reshape((dataSize[1],numPixels))
        #otherImage=otherImage[:,np.random.permutation(numPixels)]
        #otherImage=otherImage.reshape((dataSize[1],dataSize[0],dataSize[2],dataSize[3])).permute((1,0,2,3))
        
        mergeStrength=mergeStrengthMax*torch.rand((dataSize[0],1,dataSize[2],dataSize[3]))
        mergeStrength=mergeStrength.expand_as(data)
    
        data=(1.0-mergeStrength)*data+mergeStrength*otherImage

    return data

def mix_samples(data, contrastMin, contrastMax):
    #mix each sample with a copy of another sample from the same batch
    otherImage=data[torch.randperm(data.size(0)),:]
    contrast=(contrastMax-contrastMin)*torch.rand((data.size(0),1,1,1)).to(data.device)+contrastMin
    data=(1-contrast)*data + contrast*otherImage
    return data


def mixup_data(data, labels, alpha=1.0):
    #function modified from https://github.com/FrancescoPinto/RegMixup
    if alpha > 0:
        mixupProportion = np.random.beta(alpha, alpha)
    else:
        mixupProportion = 1 
    randIdx=torch.randperm(data.size(0))
    otherImage=data[randIdx,:]
    otherLabels=labels[randIdx]
    mixedData = mixupProportion * data + (1.0 - mixupProportion) * otherImage 
    return mixedData, otherLabels, mixupProportion


def scramble_pixels(data):
    #create a batch of images with pixels randomly chosen from other images in the batch
    dataSize=torch.as_tensor(data.size()).numpy()
    
    # scamble the pixels in each original image
    numPixels=dataSize[2:].prod()
    randVals=data.reshape((dataSize[0],dataSize[1],numPixels))
    #randVals=randVals[:,:,np.random.permutation(numPixels)]
    randVals=randVals[:,:,torch.randperm(numPixels)]
    randVals=randVals.reshape((dataSize[0],dataSize[1],dataSize[2],dataSize[3]))

    return randVals


def random_fourier_phase(data):
    #randomise the phase, in the fourier domain, of a batch of images: 
    #removes category information while retaining low-level properties 
    #same technique used in psychophysical experiments on human perception
    for i in range(data.size(0)):
        # perform fourier transform
        imfft=torch.fft.fft2(data[i])
        magnitude=torch.sqrt(imfft.real**2 + imfft.imag**2)
        phase=torch.arctan2(imfft.imag, imfft.real)

        # create randomised phase
        phase = (torch.rand_like(phase)-0.5)*2.0*np.pi

        # perform inverse fourier transform to reconstruct image and use this to replace original one
        data[i]=torch.fft.ifft2(magnitude * torch.exp(phase*1j) )
        
    #scale the result into the range [0,1]
    data-=torch.amin(data,dim=(1,2,3),keepdim=True)
    data/=torch.amax(data,dim=(1,2,3),keepdim=True)
    #clip the result into the range [0,1]
    #data = torch.max(torch.tensor(0.0),data)
    #data = torch.min(torch.tensor(1.0),data)
    return data


def random_fourier_magnitude(data):
    #randomise the magnitude, in the fourier domain, of a batch of images: 
    #keeps category information (object shape) while introducing noise in amplitude (texture)
    for i in range(data.size(0)):
        # perform fourier transform
        imfft=torch.fft.fft2(data[i])
        magnitude=torch.sqrt(imfft.real**2 + imfft.imag**2)
        phase=torch.arctan2(imfft.imag, imfft.real)

        # create randomised magnitude
        magnitude=torch.rand_like(magnitude)*(magnitude.max()-magnitude.min())+magnitude.min()
        #magnitude+=10*torch.randn_like(magnitude)
        #magnitude+=50*torch.rand(1).to(magnitude.device)*torch.randn_like(magnitude)
        #magnitude=scramble_pixels(magnitude[None,:,:,:])
        #phase+=1.0*torch.randn_like(phase)

        # perform inverse fourier transform to reconstruct image and use this to replace original one
        data[i]=torch.fft.ifft2(magnitude * torch.exp(phase*1j) )
        
    #scale the result into the range [0,1]
    #data-=torch.amin(data,dim=(1,2,3),keepdim=True)
    #data/=torch.amax(data,dim=(1,2,3),keepdim=True)
    #clip the result into the range [0,1]
    data = torch.max(torch.tensor(0.0),data)
    data = torch.min(torch.tensor(1.0),data)
    return data


def mix_fourier_phase(data):
    #create new images that have a mixture of the phase, in the fourier domain, of two image in the batch: 
    #removes category information while retaining low-level properties as used in psychophysical experiments on human perception
    magnitude=torch.zeros_like(data)
    phase=torch.zeros_like(data)
    for i in range(data.size(0)):
        # perform fourier transform
        imfft=torch.fft.fft2(data[i])
        magnitude[i,:,:,:]=torch.sqrt(imfft.real**2 + imfft.imag**2)
        phase[i,:,:,:]=torch.arctan2(imfft.imag, imfft.real)

    # create randomised phase
    phase=mix_samples(phase, 0, 1)
        
    for i in range(data.size(0)):
        # perform inverse fourier transform to reconstruct image and use this to replace original one
        data[i]=torch.fft.ifft2(magnitude[i] * torch.exp(phase[i]*1j) )
        
    #scale the result into the range [0,1]
    data-=torch.amin(data,dim=(1,2,3),keepdim=True)
    data/=torch.amax(data,dim=(1,2,3),keepdim=True)
    #clip the result into the range [0,1]
    #data = torch.max(torch.tensor(0.0),data)
    #data = torch.min(torch.tensor(1.0),data)
    return data


def mix_fourier_magnitude(data):
    #create new images that have a mixture of the magniture, in the fourier domain, of two image in the batch: 
    #keeps category information (object shape) while introducing noise in amplitude (texture)
    magnitude=torch.zeros_like(data)
    phase=torch.zeros_like(data)
    for i in range(data.size(0)):
        # perform fourier transform
        imfft=torch.fft.fft2(data[i])
        magnitude[i,:,:,:]=torch.sqrt(imfft.real**2 + imfft.imag**2)
        phase[i,:,:,:]=torch.arctan2(imfft.imag, imfft.real)

    # create randomised magniture
    magnitude=mix_samples(magnitude, 0, 1)

    for i in range(data.size(0)):
        # perform inverse fourier transform to reconstruct image and use this to replace original one
        data[i]=torch.fft.ifft2(magnitude[i] * torch.exp(phase[i]*1j) )
        
    #scale the result into the range [0,1]
    data-=torch.amin(data,dim=(1,2,3),keepdim=True)
    data/=torch.amax(data,dim=(1,2,3),keepdim=True)
    #clip the result into the range [0,1]
    #data = torch.max(torch.tensor(0.0),data)
    #data = torch.min(torch.tensor(1.0),data)
    return data


from pytorch_wavelets import DWTForward, DWTInverse
def jitter_dwt(data, wavelet='db3', padding='reflect', depthMax=4, scale=0.5, noise=0.05):
    numSamples=data.size(0)

    depth=torch.randint(0,depthMax,[1])
    if data.device==torch.device("cpu"):
        xfm = DWTForward(J=depth, wave=wavelet, mode=padding)
        ifm = DWTInverse(wave=wavelet, mode=padding)
    else:
        xfm = DWTForward(J=depth, wave=wavelet, mode=padding).cuda()
        ifm = DWTInverse(wave=wavelet, mode=padding).cuda()

    Yl, Yh = xfm(data)

    Yl*=scale*torch.rand(numSamples)[:,None,None,None].to(data.device) 
    Yl+=noise*torch.rand_like(Yl)
    #Yl=dwt_rand_coef(Yl, torch.rand(data.size(0))[:,None,None,None].to(data.device))
    for d in range(depth):
        Yh[d]*=scale*torch.rand(numSamples)[:,None,None,None,None].to(data.device) 
        #Yh[d]+=noise*torch.rand_like(Yh[d])
        #Yh[d]=dwt_rand_coef(Yh[d], torch.rand(numSamples)[:,None,None,None,None].to(data.device))
        #for i in range(3):
        #    Yh[d][:,:,i,:,:]*=scale*torch.rand(numSamples)[:,None,None,None].to(data.device) #**0.5
            #Yh[d][:,:,i,:,:]=dwt_rand_coef(Yh[d][:,:,i,:,:], torch.rand(numSamples)[:,None,None,None].to(data.device))

    data = ifm((Yl, Yh))

    #scale the result into the range [0,1]
    data-=torch.amin(data,dim=(1,2,3),keepdim=True)
    data/=torch.amax(data,dim=(1,2,3),keepdim=True)        
    #clip the result into the range [0,1]
    #data = torch.max(torch.tensor(0.0),data)
    #data = torch.min(torch.tensor(1.0),data)
    
    return data
















import torchvision
# define experiment
from expt_def import expt_def
netType, task, dataOOD, dataAug, actFunc, intFunc, normFunc, poolFunc, lossFunc, balanceMethod, fileName=expt_def()
from load_dataset import load_dataset
_, _, imDims, _, _, _ = load_dataset(task,1,False)
###########################################################################
#Remaining code taken/adapted from https://github.com/andyzoujm/pixmix
###########################################################################
from PIL import Image, ImageOps, ImageEnhance

IMAGE_SIZE = imDims[-1]
def int_parameter(level, maxval):
  """Helper function to scale `val` between 0 and maxval .
  Args:
    level: Level of the operation that will be between [0, `PARAMETER_MAX`].
    maxval: Maximum value that the operation can have. This will be scaled to
      level/PARAMETER_MAX.
  Returns:
    An int that results from scaling `maxval` according to `level`.
  """
  return int(level * maxval / 10)


def float_parameter(level, maxval):
  """Helper function to scale `val` between 0 and maxval.
  Args:
    level: Level of the operation that will be between [0, `PARAMETER_MAX`].
    maxval: Maximum value that the operation can have. This will be scaled to
      level/PARAMETER_MAX.
  Returns:
    A float that results from scaling `maxval` according to `level`.
  """
  return float(level) * maxval / 10.


def sample_level(n):
  return np.random.uniform(low=0.1, high=n)


def autocontrast(pil_img, _):
  return ImageOps.autocontrast(pil_img)


def equalize(pil_img, _):
  return ImageOps.equalize(pil_img)


def posterize(pil_img, level):
  level = int_parameter(sample_level(level), 4)
  return ImageOps.posterize(pil_img, 4 - level)


def rotate(pil_img, level):
  degrees = int_parameter(sample_level(level), 30)
  if np.random.uniform() > 0.5:
    degrees = -degrees
  return pil_img.rotate(degrees, resample=Image.BILINEAR)


def solarize(pil_img, level):
  level = int_parameter(sample_level(level), 256)
  return ImageOps.solarize(pil_img, 256 - level)


def shear_x(pil_img, level):
  level = float_parameter(sample_level(level), 0.3)
  if np.random.uniform() > 0.5:
    level = -level
  return pil_img.transform((IMAGE_SIZE, IMAGE_SIZE),
                           Image.AFFINE, (1, level, 0, 0, 1, 0),
                           resample=Image.BILINEAR)


def shear_y(pil_img, level):
  level = float_parameter(sample_level(level), 0.3)
  if np.random.uniform() > 0.5:
    level = -level
  return pil_img.transform((IMAGE_SIZE, IMAGE_SIZE),
                           Image.AFFINE, (1, 0, 0, level, 1, 0),
                           resample=Image.BILINEAR)


def translate_x(pil_img, level):
  level = int_parameter(sample_level(level), IMAGE_SIZE / 3)
  if np.random.random() > 0.5:
    level = -level
  return pil_img.transform((IMAGE_SIZE, IMAGE_SIZE),
                           Image.AFFINE, (1, 0, level, 0, 1, 0),
                           resample=Image.BILINEAR)


def translate_y(pil_img, level):
  level = int_parameter(sample_level(level), IMAGE_SIZE / 3)
  if np.random.random() > 0.5:
    level = -level
  return pil_img.transform((IMAGE_SIZE, IMAGE_SIZE),
                           Image.AFFINE, (1, 0, 0, 0, 1, level),
                           resample=Image.BILINEAR)


# operation that overlaps with ImageNet-C's test set
def color(pil_img, level):
    level = float_parameter(sample_level(level), 1.8) + 0.1
    return ImageEnhance.Color(pil_img).enhance(level)


# operation that overlaps with ImageNet-C's test set
def contrast(pil_img, level):
    level = float_parameter(sample_level(level), 1.8) + 0.1
    return ImageEnhance.Contrast(pil_img).enhance(level)


# operation that overlaps with ImageNet-C's test set
def brightness(pil_img, level):
    level = float_parameter(sample_level(level), 1.8) + 0.1
    return ImageEnhance.Brightness(pil_img).enhance(level)


# operation that overlaps with ImageNet-C's test set
def sharpness(pil_img, level):
    level = float_parameter(sample_level(level), 1.8) + 0.1
    return ImageEnhance.Sharpness(pil_img).enhance(level)


augmentations = [
    autocontrast, equalize, posterize, rotate, solarize, shear_x, shear_y,
    translate_x, translate_y
]

augmentations_all = [
    autocontrast, equalize, posterize, rotate, solarize, shear_x, shear_y,
    translate_x, translate_y, color, contrast, brightness, sharpness
]

def get_ab(beta):
  if np.random.random() < 0.5:
    a = np.float32(np.random.beta(beta, 1))
    b = np.float32(np.random.beta(1, beta))
  else:
    a = 1 + np.float32(np.random.beta(1, beta))
    b = -np.float32(np.random.beta(1, beta))
  return a, b

def add(img1, img2, beta):
  a,b = get_ab(beta)
  img1, img2 = img1 * 2 - 1, img2 * 2 - 1
  out = a * img1 + b * img2
  return (out + 1) / 2

def multiply(img1, img2, beta):
  a,b = get_ab(beta)
  img1, img2 = img1 * 2, img2 * 2
  out = (img1 ** a) * (img2.clip(1e-37) ** b)
  return out / 2

mixings = [add, multiply]

def pixmix(origBatch, mixing_picBatch, k=4, beta=3):
  to_tensor = torchvision.transforms.ToTensor()
  normalize = torchvision.transforms.Normalize([0.5] * 3, [0.5] * 3)
  preprocess={'normalize': normalize, 'tensorize': to_tensor}
  tensorize, normalize = preprocess['tensorize'], preprocess['normalize']

  mixedBatch=origBatch
  for img in range(np.size(origBatch,0)):
      orig=torchvision.transforms.functional.to_pil_image(origBatch[img,:])
      mixing_pic=torchvision.transforms.functional.to_pil_image(mixing_picBatch[img,:])
      if np.random.random() < 0.5:
        mixed = tensorize(augment_input(orig))
      else:
        mixed = tensorize(orig)
      
      for _ in range(np.random.randint(k + 1)):
        
        if np.random.random() < 0.5:
          aug_image_copy = tensorize(augment_input(orig))
        else:
          aug_image_copy = tensorize(mixing_pic)
    
        mixed_op = np.random.choice(mixings)
        mixed = mixed_op(mixed, aug_image_copy, beta)
        mixed = torch.clip(mixed, 0, 1)
        mixedBatch[img]=mixed #normalize(mixed)
  return mixedBatch

def augment_input(image,aug_severity=3):
  #aug_list = augmentations_all
  aug_list = augmentations
  op = np.random.choice(aug_list)
  return op(image.copy(), aug_severity)
