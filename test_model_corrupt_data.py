#############################################################################
# Code to test a model's ability to classify corrupt test images
# Performance is evaluated using percent accuracy
# Code modified from https://github.com/tanimutomo/cifar10-c-eval
#
# (c) 2023 Michael William Spratling
#############################################################################

import torch
import load_dataset
from utils import import_weights
device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")

#############################################################################
# define experiment to perform
#############################################################################
from expt_def import expt_def
netType, task, dataOOD, dataAug, actFunc, intFunc, normFunc, poolFunc, lossFunc, balanceMethod, fileName=expt_def()

batchSize=128
if task.startswith('MNIST'):
    corruptions=load_dataset.corruptionsMNIST
elif task.startswith('CIFAR'):
    corruptions=load_dataset.corruptionsCIFAR
elif task.startswith('TinyImageNet'):
    corruptions=load_dataset.corruptionsTinyImageNet
showExemplars=False
_, _, imDims, numClasses, _, _ = load_dataset(task,batchSize)

#############################################################################
# define the NN architecture to be tested
#############################################################################
#allow choice of model to be assessed, made by the arguments in "expt_def.py", 
#to be overwritten by a filename inputted as a command-line option to this function
import argparse
from model_defs import import_model, define_model
ap = argparse.ArgumentParser()
ap.add_argument("-f", "--file", required=False, help="file to process (over-riding expt_def)")
args = vars(ap.parse_args())
if args['file']!=None:
    fileName=args['file']
    model = import_model(netType, actFunc, numClasses, device, fileName)
else:
    model = define_model(netType, actFunc, intFunc, normFunc, poolFunc, imDims, numClasses, task, device)
    model = import_weights(fileName, model, device)
model.eval() # prepare model for testing

#############################################################################
# test accuracy of model on clean images 
#############################################################################
from utils import test_network_accuracy
from load_dataset import load_dataset
train_loader, test_loader, imDims, numClasses, classNames, _ = load_dataset(task,batchSize,False)
print("Evaluating Robustness to Image Corruptions of network with clean accuracy:")
test_network_accuracy(numClasses, test_loader, model, False)

#############################################################################
# test accuracy of model on corrupted images 
#############################################################################
allResults=torch.tensor([])

for corruption in corruptions:
    # load dataset
    if task.startswith('MNIST'):
        _, loader, _, _, _, _ = load_dataset('MNISTC',batchSize,False,showExemplars,None,corruption)
    elif task.startswith('CIFAR100'):
        _, loader, _, _, _, _ = load_dataset('CIFAR100C',batchSize,False,showExemplars,None,corruption)
    elif task.startswith('CIFAR10'):
        _, loader, _, _, _, _ = load_dataset('CIFAR10C',batchSize,False,showExemplars,None,corruption)
    elif task.startswith('TinyImageNet'):
        _, loader, _, _, _, _ = load_dataset('TinyImageNetC',batchSize,False,showExemplars,None,corruption)

    # test prediction accuracy
    print(corruption)
    accuracyCorruption=test_network_accuracy(numClasses, loader, model, False)
    allResults = torch.cat((allResults, torch.tensor([accuracyCorruption])), 0)
    
#############################################################################
# report the results
#############################################################################
from utils import pasteable_array
print('All results:\n')
pasteable_array(allResults)
print('Average Accuracy on Corrupted Data: %.2f %%\n' % (allResults.mean()))
