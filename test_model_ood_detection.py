#############################################################################
# Code to test a model's ability to reject test images from unknown classes 
# (i.e  to distinguish IID and OOD data)
# Performance is evaluated using the AUROC on a per-OOD dataset basis.
#
# (c) 2023 Michael William Spratling
#############################################################################

import numpy as np
import torch
import torch.nn.functional as F
from sklearn.metrics import roc_auc_score
from load_dataset import load_dataset
from utils import import_weights
device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")

#############################################################################
# define experiment to perform
#############################################################################
from expt_def import expt_def
netType, task, dataOOD, dataAug, actFunc, intFunc, normFunc, poolFunc, lossFunc, balanceMethod, fileName=expt_def()
batchSize=200  #define number of images in test set batch
showExemplars=False

#define OOD data sets to be used
if task.startswith('MNIST'):
    #tasksOOD=['SynthGaussian','SynthBernoulli']
    tasksOOD=['Omniglot','FashionMNIST','KMNIST','AugPhase','AugScramble','SynthBlobs','SynthUniform']
elif task.startswith('SVHN'):
    tasksOOD=['Textures','Icons50','CIFAR10','AugPhase','AugScramble','SynthBlobs','SynthUniform']
elif task.startswith('CIFAR100'):
    tasksOOD=['Textures','SVHN','CIFAR10','AugPhase','AugScramble','SynthBlobs','SynthUniform']
elif task.startswith('CIFAR10'):
    tasksOOD=['Textures','SVHN','CIFAR100','AugPhase','AugScramble','SynthBlobs','SynthUniform']
elif task.startswith('TinyImageNet'):
    tasksOOD=['Textures','iNaturalist','ImageNetO','AugPhase','AugScramble','SynthBlobs','SynthUniform']
elif task.startswith('ImageNet'):
    tasksOOD=['Textures','iNaturalist','ImageNetO','AugPhase','AugScramble','SynthBlobs','SynthUniform']
    batchSize=50
_, _, imDims, numClasses, _, _ = load_dataset(task,batchSize)

#############################################################################
# define the NN architecture to be tested
#############################################################################
#allow choice of model to be assessed, made by the arguments in "expt_def.py", 
#to be overwritten by a filename inputted as a command-line option to this function
import argparse
from model_defs import import_model, define_model
ap = argparse.ArgumentParser()
ap.add_argument("-f", "--file", required=False, help="file to process (over-riding expt_def)")
args = vars(ap.parse_args())
if args['file']!=None:
    fileName=args['file']
    model = import_model(netType, actFunc, numClasses, device, fileName)
else:
    model = define_model(netType, actFunc, intFunc, normFunc, poolFunc, imDims, numClasses, task, device)
    model = import_weights(fileName, model, device)
model.eval() # prepare model for testingstate

#############################################################################
# define method used to determine is model accepts of rejects samples
#############################################################################
from augment_data import random_fourier_phase, scramble_pixels
def measure_acceptance(model,test_loader,augment=None):
    #Uses post-hoc methods to calculate the model's prediction
    #(or confidence) for each sample being of a known or unknown class 
    #(i.e in- or out-of-distribution). In all cases, higher output corresponds
    #to a more confident prediction that sample is in-distribution.
    
    device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")
    model.to(device)

    # initialize variables to record metrics
    maxLogits=torch.tensor([]).to(device)
    maxSoftmax=torch.tensor([]).to(device)
    energy=torch.tensor([]).to(device)
    tempVal=1.0
    GEN=torch.tensor([]).to(device)
    gamma=0.1
    M=200 #100 gives mixed results depending on loss function
    SepScore=torch.tensor([]).to(device)
    correctPred=torch.tensor([]).to(device)
    for samples, labels in test_loader:
        samples, labels = samples.to(device), labels.to(device)
        
        if augment=='AugPhase':
            samples=random_fourier_phase(samples)
        elif augment=='AugScramble':
            samples=scramble_pixels(samples)

        # forward pass: compute predicted outputs by passing inputs to the model
        y = model(samples).detach()

        # record MLS for each sample: 
        # the maximum response of the network measured from the logits
        maxLogitsBatch, pred = torch.max(y, 1)
        maxLogits=torch.cat((maxLogits,maxLogitsBatch))

        # record MSP for each sample: 
        # the maximum response of the network measured from the softmax output
        maxSoftmaxBatch=torch.max(F.softmax(y, dim=1), 1).values
        maxSoftmax=torch.cat((maxSoftmax,maxSoftmaxBatch))
        
        # record the Energy score for each sample:
        energyBatch = tempVal * torch.logsumexp(y/tempVal, dim=1)
        energy=torch.cat((energy,energyBatch))

        # record the GEN score for each sample:
        #M=int(0.1*y.size(1)) # 0.1*numClasses - v.poor with 10 classes!
        probsSorted = torch.sort(F.softmax(y, dim=1), dim=1).values
        probsSorted = probsSorted[:,-M:] #take top-M probs
        GENBatch = -torch.sum(probsSorted**gamma * (1 - probsSorted)**(gamma), dim=1)
        GEN=torch.cat((GEN,GENBatch))

        # record the difference between the maximum and second highest responses for each sample:
        ySort=torch.sort(y, dim=1, descending=True).values
        excess=F.relu(0.05+ySort-maxLogitsBatch[:,None])
        #SepScoreBatch=maxLogitsBatch-torch.max(excess[:,1:], dim=1).values
        #SepScoreBatch=excess[:,0]/torch.maximum(torch.tensor([1e-6]).to(device),torch.sum(excess, dim=1))
        SepScoreBatch=maxLogitsBatch*torch.exp(-10.*torch.sum(excess[:,1:], dim=1))
        #SepScoreBatch=ySort[:,0]-torch.mean(ySort[:,1:], dim=1)    
        #SepScoreBatch=ySort[:,0]-torch.sum(ySort[:,1:]/(torch.arange(1,ySort.size(1))).to(ySort.device), dim=1)    
        SepScore=torch.cat((SepScore,SepScoreBatch))

        #record if prediction is correct
        correctPredBatch=(pred==labels)
        correctPred=torch.cat((correctPred,correctPredBatch))


    print('num samples=',len(maxLogits))
    return maxLogits, maxSoftmax, energy, GEN, SepScore, correctPred

import matplotlib.pyplot as plt
plt.style.use('seaborn-v0_8-talk')
def plot_confidence_histograms(acceptPred, titleText):
    plt.figure()
    plt.hist(np.array(acceptPred.to(torch.device("cpu"))), bins = np.arange(0,1.05,0.05))
    plt.title('confidence for %s data' % titleText)

def plot_joint_confidence_histograms(acceptPred1, legendText1, acceptPred2, legendText2, actType='act', fileName=None):
    #define appearance
    #plt.style.use('seaborn-v0_8-poster')
    fontSize=42
    color1='tab:blue'
    color2='tab:orange'
    #plt.rcParams['font.size']=30
    #plt.rcParams.update({'font.size': 50})
    fig, ax1 = plt.subplots()
    ax2 = ax1.twinx()

    #define how to bin data
    acceptPred1=np.array(acceptPred1.to(torch.device("cpu")))
    acceptPred2=np.array(acceptPred2.to(torch.device("cpu")))
    binMax=1.05*acceptPred1.max()
    binMin=0.9*acceptPred2.min()
    if binMin<0.1*binMax:
        binMin=0
    bins = np.arange(binMin,binMax,0.01*(acceptPred1.max()-acceptPred2.min()))

    #plot histograms
    #plt.hist([acceptPred1,acceptPred2], bins, label=[legendText1, legendText2])
    ax2.hist(acceptPred1, bins, label=legendText1, alpha=0.5, color=color2, rwidth=1)
    ax1.hist(acceptPred2, bins, label=legendText2, alpha=0.5, color=color1, rwidth=1)
    #fig.legend(loc='outside upper right', bbox_to_anchor=(0.925, 1.035), ncols=2, fancybox=True, fontsize = fontSize)

    #set appearance of axes
    ax1.set_xlabel('Activation Strength', fontsize = fontSize)
    ax2.set_ylabel(legendText1, fontsize = fontSize, color=color2, backgroundcolor='white', y=1, labelpad=0, ha='right', va='top', rotation="horizontal")
    ax1.set_ylabel(legendText2, fontsize = fontSize, color=color1, backgroundcolor='white', y=1, labelpad=0, ha='left', va='top', rotation="horizontal")
    ax2.locator_params(axis='both', nbins=5)   
    ax1.locator_params(axis='both', nbins=5)   
    ax2.tick_params(labelsize=0.9*fontSize)
    ax1.tick_params(labelsize=0.9*fontSize)
    ax2.tick_params(axis='y', colors=color2)
    ax1.tick_params(axis='y', colors=color1)
    ax1.set_ylim(top=ax1.get_ylim()[1]*1.1)
    if fileName is not None:
        plt.savefig(fileName+'_'+actType+'_hist_vs'+legendText2+'.pdf',format='pdf',bbox_inches='tight')
    plt.show()

#############################################################################
# test model with in-distribution data
#############################################################################
#load IID dataset
train_loader, test_loader, imDims, numClasses, classNames, _ = load_dataset(task,batchSize,False,showExemplars)

#test model
acceptPredforIIDdataMLS,acceptPredforIIDdataMSP,acceptPredforIIDdataEnergy,acceptPredforIIDdataGEN,acceptPredforIIDdataSep,correctPred = measure_acceptance(model,test_loader)
acceptGTforIIDdata=np.ones(len(acceptPredforIIDdataMLS))
print("Clean accuracy (%)=",100.0*torch.sum(correctPred)/len(correctPred))
#plot_confidence_histograms(acceptPredforIIDdataMSP, 'IID')
plot_joint_confidence_histograms(acceptPredforIIDdataMLS[correctPred==1], 'Correct', acceptPredforIIDdataMLS[correctPred==0], 'Wrong', 'MLS', fileName)
plot_joint_confidence_histograms(acceptPredforIIDdataMSP[correctPred==1], 'Correct', acceptPredforIIDdataMSP[correctPred==0], 'Wrong', 'MSP', fileName)

#############################################################################
# test model with out-of-distribution data
#############################################################################

allResultsMLS=torch.tensor([])
allResultsMSP=torch.tensor([])
allResultsEnergy=torch.tensor([])
allResultsGEN=torch.tensor([])
allResultsSep=torch.tensor([])
for taskOOD in tasksOOD:
    #load dataset
    if taskOOD.startswith('Aug'):
        _, ood_loader, _, _, _, _ = load_dataset(task,batchSize,False,False)
        print(taskOOD)
    else:
        _, ood_loader, _, _, _, _ = load_dataset(taskOOD,batchSize,False,showExemplars,imDimsReqd=list(imDims))
    
    #test model
    acceptPredforOODdataMLS,acceptPredforOODdataMSP,acceptPredforOODdataEnergy,acceptPredforOODdataGEN,acceptPredforOODdataSep,_ = measure_acceptance(model,ood_loader,taskOOD)
    #plot_confidence_histograms(acceptPredforOODdataMSP, taskOOD)
    plot_joint_confidence_histograms(acceptPredforIIDdataMLS, task, acceptPredforOODdataMLS, taskOOD, 'MLS', fileName)
    plot_joint_confidence_histograms(acceptPredforIIDdataMSP, task, acceptPredforOODdataMSP, taskOOD, 'MSP', fileName)
    acceptGTforOODdata=np.zeros(len(acceptPredforOODdataMLS))
    acceptGT=np.append(acceptGTforIIDdata,acceptGTforOODdata)
    
    #############################################################################
    # report the results for individual datasets
    #############################################################################
    
    aurocMLS = 100*roc_auc_score(acceptGT, np.append(acceptPredforIIDdataMLS.cpu(),acceptPredforOODdataMLS.cpu()))
    print("% AUROC for MLS", aurocMLS)
    allResultsMLS = torch.cat((allResultsMLS, torch.tensor([aurocMLS])), 0)

    aurocMSP = 100*roc_auc_score(acceptGT, np.append(acceptPredforIIDdataMSP.cpu(),acceptPredforOODdataMSP.cpu()))
    print("% AUROC for MSP", aurocMSP)
    allResultsMSP = torch.cat((allResultsMSP, torch.tensor([aurocMSP])), 0)

    aurocEnergy = 100*roc_auc_score(acceptGT, np.append(acceptPredforIIDdataEnergy.cpu(),acceptPredforOODdataEnergy.cpu()))
    print("% AUROC for Energy", aurocEnergy)
    allResultsEnergy = torch.cat((allResultsEnergy, torch.tensor([aurocEnergy])), 0)

    aurocGEN = 100*roc_auc_score(acceptGT, np.append(acceptPredforIIDdataGEN.cpu(),acceptPredforOODdataGEN.cpu()))
    print("% AUROC for GEN", aurocGEN)
    allResultsGEN = torch.cat((allResultsGEN, torch.tensor([aurocGEN])), 0)

    aurocSep = 100*roc_auc_score(acceptGT, np.append(acceptPredforIIDdataSep.cpu(),acceptPredforOODdataSep.cpu()))
    print("% AUROC for Sep", aurocSep)
    allResultsSep = torch.cat((allResultsSep, torch.tensor([aurocSep])), 0)

    print("\n",flush=True)
    
#############################################################################
# report the results for all datasets
#############################################################################
from utils import pasteable_array
print('All results for the OOD datasets:',tasksOOD)

print('% AUROC for MLS:')
pasteable_array(allResultsMLS)
print('Average AUROC for MLS %.2f %%' % (allResultsMLS.mean()))

print('% AUROC for MSP:')
pasteable_array(allResultsMSP)
print('Average AUROC for MSP %.2f %%' % (allResultsMSP.mean()))

print('% AUROC for Energy:')
pasteable_array(allResultsEnergy)
print('Average AUROC for Energy %.2f %%' % (allResultsEnergy.mean()))

print('% AUROC for GEN:')
pasteable_array(allResultsGEN)
print('Average AUROC for GEN %.2f %%' % (allResultsGEN.mean()))

print('% AUROC for Sep:')
pasteable_array(allResultsSep)
print('Average AUROC for Sep %.2f %%' % (allResultsSep.mean()))

print("\n",flush=True)

