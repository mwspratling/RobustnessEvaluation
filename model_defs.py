#############################################################################
# Code that defines neural network architectures
#
# (c) 2023 Michael William Spratling
#############################################################################

import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
from activation_functions import define_activation_function
from integration_functions import define_integration_function
from norm_functions import define_norm_function
from pooling_functions import define_pooling_function
from utils import import_weights, calc_CNN_layer_sizes, print_stats, parse_string


def define_model(netType, actFunc, intFunc, normFunc, poolFunc, imDims, numClasses, task, device, inputNorm=None):
    #defines model using this codebase
    if netType.startswith('MLP'):
        numNodes=parse_string(netType,'MLP')
        if len(numNodes)==0:
            numNodes=[500,500,500] #default number of nodes in each hidden layer
        else:
            for i in np.arange(len(numNodes)):
                numNodes[i]=int(numNodes[i])
        numNodes=np.append(numNodes,int(numClasses))
        model = MLP(imDims, numNodes, actFunc=actFunc)
    elif netType.startswith('ConvNet'):
        numMasks, maskSize, stride, padding, poolSize = hyperparams_ConvNet(netType,imDims,numClasses)
        mapDims=calc_CNN_layer_sizes(imDims,numMasks,maskSize,padding,stride,poolSize)
        model = ConvNet(imDims, numMasks, maskSize, stride, padding, poolSize, mapDims, actFunc=actFunc, intFunc=intFunc, normFunc=normFunc, poolFunc=poolFunc, inputNorm=inputNorm)
    elif netType.startswith('WideResNet'):
        #from wideresnet import WideResNet #code import from Bag-of-Tricks-for-AT - will ignore specification given in "expt_def.py" of intFunc, normFunc and poolFunc and require code to be modified to accomodate differences with model definition given in this code
        depthWidth=parse_string(netType,'WideResNet')
        depth=int(depthWidth[0])
        width=int(depthWidth[1])
        model = WideResNet(num_classes=int(numClasses), inChannels=imDims[0], depth=depth, widen_factor=width, actFunc=actFunc, intFunc=intFunc, normFunc=normFunc)
    elif netType.startswith('ResNet'):
        #from resnet import ResNet, BasicBlock, Bottleneck #code import from Bag-of-Tricks-for-AT - will ignore specification given in "expt_def.py" of intFunc, normFunc and poolFunc and require code to be modified to accomodate differences with model definition given in this code
        depth=int(netType.split('ResNet')[1])
        if depth==18:
            config=[2, 2, 2, 2]
            block=BasicBlock
        elif depth==34:
            config=[3, 4, 6, 3]
            block=BasicBlock
        elif depth==50:
            config=[3, 4, 6, 3]
            block=Bottleneck
        elif depth==101:
            config=[3, 4, 23, 3]
            block=Bottleneck
        elif depth==152:
            config=[3, 8, 36, 3]
            block=Bottleneck
        model = ResNet(block, config, num_classes=int(numClasses), inChannels=imDims[0], actFunc=actFunc, intFunc=intFunc, normFunc=normFunc)
    elif netType.startswith('SResNet'):
        depth=int(netType.split('SResNet')[1])
        if depth==20:
            config=[3, 3, 3]
        if depth==32:
            config=[5, 5, 5]    
        if depth==44:
            config=[7, 7, 7]    
        if depth==56:
            config=[9, 9, 9]    
        if depth==110:
            config=[18, 18, 18]
        if depth==1202:
            config=[200, 200, 200]    
        model = SResNet(SBasicBlock, config, num_classes=int(numClasses), inChannels=imDims[0], actFunc=actFunc, intFunc=intFunc, normFunc=normFunc)
    elif netType.startswith('PreActResNet'):
        #from preactresnet import PreActResNet, PreActBlock, PreActBottleneck #code import from Bag-of-Tricks-for-AT - will ignore specification given in "expt_def.py" of intFunc, normFunc and poolFunc and require code to be modified to accomodate differences with model definition given in this code
        depth=int(netType.split('PreActResNet')[1])
        if depth==18:
            config=[2, 2, 2, 2]
            block=PreActBlock
        elif depth==34:
            config=[3, 4, 6, 3]
            block=PreActBlock
        elif depth==50:
            config=[3, 4, 6, 3]
            block=PreActBottleneck
        elif depth==101:
            config=[3, 4, 23, 3]
            block=PreActBottleneck
        elif depth==152:
            config=[3, 8, 36, 3]
            block=PreActBottleneck
        model = PreActResNet(block, config, num_classes=int(numClasses), inChannels=imDims[0], actFunc=actFunc, intFunc=intFunc, normFunc=normFunc)
    elif netType.startswith('RB'):
        TrainingMethod=parse_string(netType,'RB')[0]
        netType=netType.split('RB'+TrainingMethod+'-')[1]
        print(netType,TrainingMethod)
        #import pretrained model robust bench model zoo
        from robustbench import load_model
        model = load_model(model_name=netType, dataset=task.lower(), threat_model=TrainingMethod)
        print('***Imported robustbench checkpoint with pre-trained weights - no need to import weights subsequently***')
    model.to(device)
    return model


def import_model(netType, actFunc, numClasses, device, fileName):
    #imports model from another codebase

    #**Note: if training a model using code from https://github.com/P2333/Bag-of-Tricks-for-AT,
    #the function normalize (in train_cifar.py) needs to be modified to return X unmodified,
    #in order for the trained model to be compatible with this code. Checkpoints that can be
    #downloaded from the same website seem to have been trained with X unmodified,
    #so can be tested with this code.
    if fileName.startswith('wide'):
        #using checkpoint from https://github.com/P2333/Bag-of-Tricks-for-AT
        #or a wideresnet trained using code from from https://github.com/P2333/Bag-of-Tricks-for-AT**
        #or a wideresnet trained using code from from https://github.com/andyzoujm/pixmix
        #"expt_def.py" needs to correctly define the network architcture, activation
        #function and training dataset
        #from wideresnet import WideResNet #code import from Bag-of-Tricks-for-AT or pixmix - will require code to be modified to accomodate differences with model definition given in this code (particularly no loss returned from model)
        depthWidth=parse_string(netType,'WideResNet')
        depth=int(depthWidth[0])
        width=int(depthWidth[1])
        model = WideResNet(num_classes=int(numClasses), depth=depth, widen_factor=width)
        #model = WideResNet(depth, int(numClasses),width) #if using pixmix definition
        model = import_weights(fileName, model, device)
    elif fileName.startswith('resnet'):
        #using checkpoint from https://github.com/amodas/PRIME-augmentations
        #or a resnet trained using code from from https://github.com/P2333/Bag-of-Tricks-for-AT**
        #or a resnet trained using code from from https://github.com/andyzoujm/pixmix modified to use the definition of a resnet provided by Bag-of-Tricks-for-AT
        #"expt_def.py" needs to correctly define training dataset
        #from resnet import ResNet, BasicBlock #code import from Bag-of-Tricks-for-AT - will require code to be modified to accomodate differences with model definition given in this code (particularly no loss returned from model)
        depth=parse_string(fileName,'resnet','_')
        depth=int(depth[0])
        if depth==18:
            model = ResNet(BasicBlock, [2, 2, 2, 2], num_classes=int(numClasses))
        model = import_weights(fileName, model, device)

    return model



class InputNormalisationLayer(nn.Module):
     #normalises the input by subtracting the mean and dividing by the standard deviation 
     def __init__(self, inputNorm, numImageChannels):
         super(InputNormalisationLayer, self).__init__()
         if inputNorm is None:
             #default is mean=0, std=1 (i.e. no input normalisation)
             inputNorm=[[0]*numImageChannels,[1]*numImageChannels]
         self.inputMean = nn.Parameter(torch.tensor(inputNorm[0])[None,:,None,None], requires_grad=False)
         self.inputStd = nn.Parameter(torch.tensor(inputNorm[1])[None,:,None,None], requires_grad=False)

     def forward(self, x):
         x = (x - self.inputMean)/self.inputStd
         return x

#############################################################################
# Multi-Layer Perceptron (fully-connected network)
#############################################################################

class MLP(nn.Module):
    # a fully-connected feedforward network with a variable number of layers
    def __init__(self, imDims, numNodes, actFunc='ReLU', inputNorm=None):
        super(MLP, self).__init__()
        self.imDims=imDims
        self.numNodes=numNodes
        inChannels=np.append(imDims.prod(),numNodes)
        self.inputNorm=InputNormalisationLayer(inputNorm, imDims[0])
        self.fc=nn.ModuleList()
        self.actFunc=nn.ModuleList()
        for idx in range(np.size(self.numNodes)):
            self.actFunc.append(define_activation_function(actFunc, inChannels[idx]))
            self.fc.append(nn.Linear(inChannels[idx], numNodes[idx]))

    def forward(self, x, lossFunc='', labels=None, yAllReqd=False, yAllTarget=None, warpMatrix=None):
        x = self.inputNorm(x)
        # flatten image input
        x = x.view(-1, self.imDims.prod())
        # calc activation of each layer
        for idx in range(np.size(self.numNodes)):
            x = self.actFunc[idx](x)
            x = self.fc[idx](x)    
        return x

#############################################################################
# ConvNet
#############################################################################

class ConvNet(nn.Module):
    # a fully-convolutional CNN that can be customised through its input variables
    def __init__(self, imDims, numMasks, maskSize, stride, padding, poolSize, mapDims, actFunc='ReLU', intFunc='Conv', normFunc='IdentityNorm', poolFunc='AvgPool', inputNorm=None):
        super(ConvNet, self).__init__()
        self.expandInput=1
        numLayers=np.size(numMasks)
        inChannels=np.append(imDims[0]*self.expandInput,numMasks)
        mapDims=np.vstack([imDims[1:],mapDims.astype(int)])

        self.inputNorm=InputNormalisationLayer(inputNorm, imDims[0])
        self.actFunc=nn.ModuleList()
        self.normFunc=nn.ModuleList()
        self.poolFunc=nn.ModuleList()
        self.intFunc=nn.ModuleList()
        for idx in range(numLayers):
            self.actFunc.append(define_activation_function(actFunc, inChannels[idx]))
            if idx==numLayers-1 and 'DIM' in intFunc:
                #ensure final layer can produce -ve values: required for CEloss to work effectively
                self.intFunc.append(define_integration_function('Conv', inChannels[idx], numMasks[idx], maskSize[idx], stride[idx], padding[idx], bias=True, layerDims=mapDims[idx]))
            else:
                self.intFunc.append(define_integration_function(intFunc, inChannels[idx], numMasks[idx], maskSize[idx], stride[idx], padding[idx], bias=True, layerDims=mapDims[idx]))
            self.normFunc.append(define_norm_function(normFunc, numMasks[idx]))
            self.poolFunc.append(define_pooling_function(poolFunc, poolSize[idx]))
        self.numMasks=numMasks

    def forward(self, x):
        x = self.inputNorm(x)
        for idx in range(np.size(self.numMasks)):
            x = self.actFunc[idx](x)
            x = self.intFunc[idx](x)
            x = self.normFunc[idx](x)
            x = self.poolFunc[idx](x)
        y = x.view(-1, self.numMasks[-1])
        return y


def hyperparams_ConvNet(netType,imDims,numClasses):
    #defines hyperparameter values for different sized fully-convolutional
    #neural networks that are suitable for classifying standard-sized images.

    #netType is a string of the form "ConvNetL-W-N" where L, W, and N are
    #integers defining, respectively:
    #L=the number of layers,
    #W=the width and height of the convolution masks in each layer, and
    #N=the number of masks in each layer

    if imDims[-1]!=imDims[-2]:
        print('ERROR: function hyperparams_ConvNet expects square images. Need to design custom network for non-square images')
    else:
        imDims=imDims[-1]

    #parameters for networks with 3 layers
    if netType.startswith('ConvNet3-7-'):
        numNodes=netType.split('ConvNet3-7-')
        maskSize=(7,7,7)
        if imDims==28:
            padding=(0,1,0)
            poolSize=(2,1,1)
        elif imDims==32:
            padding=(0,0,0)
            poolSize=(2,1,1)
    elif netType.startswith('ConvNet3-5-'):
        numNodes=netType.split('ConvNet3-5-')
        maskSize=(5,5,5)
        if imDims==28:
            padding=(0,1,0)
            poolSize=(2,2,1)
        elif imDims==32:
            padding=(0,0,0)
            poolSize=(2,2,1)
    elif netType.startswith('ConvNet3-3-'):
        numNodes=netType.split('ConvNet3-3-')
        maskSize=(3,3,3)
        if imDims==28:
            padding=(1,0,0)
            poolSize=(2,2,4)
        elif imDims==32:
            padding=(1,0,0)
            poolSize=(2,2,5)

    #parameters for networks with 5 layers
    if netType.startswith('ConvNet5-7-'):
        numNodes=netType.split('ConvNet5-7-')
        maskSize=(7,7,7,7,1)
        if imDims==28:
            padding=(1,1,1,1,0)
            poolSize=(1,2,1,2,1)
        elif imDims==32:
            padding=(1,1,1,0,0)
            poolSize=(1,2,1,2,1)
    elif netType.startswith('ConvNet5-5-'):
        numNodes=netType.split('ConvNet5-5-')
        maskSize=(5,5,5,5,1)
        if imDims==28:
            padding=(0,0,0,0,0)
            poolSize=(1,2,1,2,1)
        elif imDims==32:
            padding=(1,1,1,0,0)
            poolSize=(1,2,2,2,1)
    elif netType.startswith('ConvNet5-3-'):
        numNodes=netType.split('ConvNet5-3-')
        maskSize=(3,3,3,3,3) #(3,3,3,3,1)
        if imDims==28:
            padding=(0,0,0,0,0)
            poolSize=(1,2,2,1,1)
        elif imDims==32:
            padding=(1,1,1,0,0)
            poolSize=(1,2,2,2,1)
        elif imDims==64:
            padding=(1,1,1,1,0)
            poolSize=(2,2,2,2,2)

    #parameters for networks with 7 layers
    if netType.startswith('ConvNet7-7-'):
        numNodes=netType.split('ConvNet7-7-')
        maskSize=(7,7,7,7,7,7,1)
        if imDims==28:
            padding=(1,1,1,1,1,0,0)
            poolSize=(1,1,1,1,1,2,1)
        elif imDims==32:
            padding=(1,1,1,0,0,0,0)
            poolSize=(1,1,1,1,1,2,1)
    elif netType.startswith('ConvNet7-5-'):
        numNodes=netType.split('ConvNet7-5-')
        maskSize=(5,5,5,5,5,5,1)
        if imDims==28:
            padding=(0,1,1,1,1,0,0)
            poolSize=(1,1,2,1,1,2,1)
        elif imDims==32:
            padding=(0,1,1,1,0,0,0)
            poolSize=(1,1,2,1,1,2,1)
    elif netType.startswith('ConvNet7-3-'):
        numNodes=netType.split('ConvNet7-3-')
        maskSize=(3,3,3,3,3,3,3)
        if imDims==28:
            padding=(0,0,1,0,0,0,0)
            poolSize=(1,1,2,1,1,2,1)
        elif imDims==32:
            padding=(0,0,1,0,0,0,0)
            poolSize=(1,1,2,1,2,1,1)

    #parameters for networks with 9 layers
    if netType.startswith('ConvNet9-7-'):
        numNodes=netType.split('ConvNet9-7-')
        maskSize=(7,7,7,7,7,7,7,7,1)
        if imDims==28:
            padding=(2,2,2,1,1,1,1,1,0)
            poolSize=(1,1,1,1,1,1,1,2,1)
        elif imDims==32:
            padding=(2,1,1,1,1,1,1,1,0)
            poolSize=(1,1,1,1,1,1,1,2,1)
    elif netType.startswith('ConvNet9-5-'):
        numNodes=netType.split('ConvNet9-5-')
        maskSize=(5,5,5,5,5,5,5,5,1)
        if imDims==28:
            padding=(1,1,1,1,1,1,1,1,0)
            poolSize=(1,1,1,2,1,1,1,2,1)
        elif imDims==32:
            padding=(1,1,1,1,1,1,1,0,0)
            poolSize=(1,1,1,2,1,1,1,2,1)
    elif netType.startswith('ConvNet9-3-'):
        numNodes=netType.split('ConvNet9-3-')
        maskSize=(3,3,3,3,3,3,3,3,3)
        if imDims==28:
            padding=(0,0,0,0,0,0,0,0,0)
            poolSize=(1,1,1,1,2,1,1,1,1)
        elif imDims==32:
            padding=(0,0,0,0,0,0,0,0,0)
            poolSize=(1,1,2,1,1,1,1,1,1)

    stride=((1,)*len(maskSize))
    numNodes=int(numNodes[1])
    numMasks=((numNodes,)*(len(maskSize)-1))+(int(numClasses),)
    return numMasks, maskSize, stride, padding, poolSize

#code to store internal network activity
activation = {}
def get_activation(name):
    #function from: https://discuss.pytorch.org/t/how-can-l-load-my-best-model-as-a-feature-extractor-evaluator/17254/6
    def hook(model, input, output):
        activation[name] = output
    return hook

#############################################################################
# ResNet - code modified from https://github.com/P2333/Bag-of-Tricks-for-AT
#############################################################################
'''ResNet in PyTorch.

For Pre-activation ResNet, see 'preact_resnet.py'.

Reference:
[1] Kaiming He, Xiangyu Zhang, Shaoqing Ren, Jian Sun
    Deep Residual Learning for Image Recognition. arXiv:1512.03385
'''

class BasicBlock(nn.Module):
    expansion = 1

    def __init__(self, in_planes, planes, stride=1, actFunc='ReLU', intFunc='Conv', normFunc='BatchNorm'):
        super(BasicBlock, self).__init__()

        self.conv1 = define_integration_function(intFunc, in_planes, planes, kernel_size=3, stride=stride, padding=1, bias=False)
        self.bn1 = define_norm_function(normFunc, planes)
        self.act1 = define_activation_function(actFunc, planes)
        self.conv2 = define_integration_function(intFunc, planes, planes, kernel_size=3, stride=1, padding=1, bias=False)
        self.bn2 = define_norm_function(normFunc, planes)

        self.shortcut = nn.Sequential()
        if stride != 1 or in_planes != self.expansion*planes:
            self.shortcut = nn.Sequential(
                define_integration_function(intFunc, in_planes, self.expansion*planes,
                          kernel_size=1, stride=stride, padding=0, bias=False),
                define_norm_function(normFunc, self.expansion*planes)
            )
        self.actOut=define_activation_function(actFunc, self.expansion*planes)

    def forward(self, x):
        out = self.act1(self.bn1(self.conv1(x)))
        out = self.bn2(self.conv2(out))
        out += self.shortcut(x)
        out = self.actOut(out)
        return out


class Bottleneck(nn.Module):
    expansion = 4

    def __init__(self, in_planes, planes, stride=1, actFunc='ReLU', intFunc='Conv', normFunc='BatchNorm'):
        super(Bottleneck, self).__init__()
        self.conv1 = define_integration_function(intFunc, in_planes, planes, kernel_size=1, stride=1, padding=0, bias=False)
        self.bn1 = define_norm_function(normFunc, planes)
        self.act1 = define_activation_function(actFunc, planes)
        self.conv2 = define_integration_function(intFunc, planes, planes, kernel_size=3, stride=stride, padding=1, bias=False)
        self.bn2 = define_norm_function(normFunc, planes)
        self.act2 = define_activation_function(actFunc, planes)
        self.conv3 = define_integration_function(intFunc, planes, self.expansion*planes, kernel_size=1, stride=1, padding=0, bias=False)
        self.bn3 = define_norm_function(normFunc, self.expansion*planes)

        self.shortcut = nn.Sequential()
        if stride != 1 or in_planes != self.expansion*planes:
            self.shortcut = nn.Sequential(
                define_integration_function(intFunc, in_planes, self.expansion*planes,
                          kernel_size=1, stride=stride, padding=0, bias=False),
                define_norm_function(normFunc, self.expansion*planes)
            )
        self.actOut=define_activation_function(actFunc, self.expansion*planes)

    def forward(self, x):
        out = self.act1(self.bn1(self.conv1(x)))
        out = self.act2(self.bn2(self.conv2(out)))
        out = self.bn3(self.conv3(out))
        out += self.shortcut(x)
        out = self.actOut(out)
        return out


class ResNet(nn.Module):
    def __init__(self, block, num_blocks, num_classes=10, inChannels=3, actFunc='ReLU', intFunc='Conv', normFunc='BatchNorm', inputNorm=None):
        super(ResNet, self).__init__()
        self.inputNorm=InputNormalisationLayer(inputNorm, inChannels)
        self.in_planes = 64
        self.conv1 = nn.Conv2d(inChannels, 64, kernel_size=3,
                               stride=1, padding=1, bias=False)
        self.bn1 = nn.BatchNorm2d(64)
        self.layer1 = self._make_layer(block, 64, num_blocks[0], stride=1, actFunc=actFunc, intFunc=intFunc, normFunc=normFunc)
        self.layer2 = self._make_layer(block, 128, num_blocks[1], stride=2, actFunc=actFunc, intFunc=intFunc, normFunc=normFunc)
        self.layer3 = self._make_layer(block, 256, num_blocks[2], stride=2, actFunc=actFunc, intFunc=intFunc, normFunc=normFunc)
        self.layer4 = self._make_layer(block, 512, num_blocks[3], stride=2, actFunc=actFunc, intFunc=intFunc, normFunc=normFunc)
        self.linear = nn.Linear(512*block.expansion, num_classes)

    def _make_layer(self, block, planes, num_blocks, stride, actFunc, intFunc, normFunc):
        strides = [stride] + [1]*(num_blocks-1)
        layers = []
        for stride in strides:
            layers.append(block(self.in_planes, planes, stride, actFunc, intFunc, normFunc))
            self.in_planes = planes * block.expansion
        return nn.Sequential(*layers)

    def forward(self, x):
        x = self.inputNorm(x)
        out = F.relu(self.bn1(self.conv1(x)))
        out = self.layer1(out)
        out = self.layer2(out)
        out = self.layer3(out)
        out = self.layer4(out)
        out = F.adaptive_avg_pool2d(out, (1, 1)) #avg_pool replaced by global average pooling to allow network to work with different image sizes
        out = out.view(out.size(0), -1)
        out = self.linear(out)
        return out

#############################################################################
# SResNet - code modified from https://github.com/akamaster/pytorch_resnet_cifar10/blob/master/resnet.py
#############################################################################
'''
Properly implemented ResNet-s for CIFAR10 as described in paper [1].
The implementation and structure of this file is hugely influenced by [2]
which is implemented for ImageNet and doesn't have option A for identity.
Moreover, most of the implementations on the web is copy-paste from
torchvision's resnet and has wrong number of params.
Proper ResNet-s for CIFAR10 (for fair comparision and etc.) has following
number of layers and parameters:
name      | layers | params
ResNet20  |    20  | 0.27M
ResNet32  |    32  | 0.46M
ResNet44  |    44  | 0.66M
ResNet56  |    56  | 0.85M
ResNet110 |   110  |  1.7M
ResNet1202|  1202  | 19.4m
which this implementation indeed has.
Reference:
[1] Kaiming He, Xiangyu Zhang, Shaoqing Ren, Jian Sun
    Deep Residual Learning for Image Recognition. arXiv:1512.03385
[2] https://github.com/pytorch/vision/blob/master/torchvision/models/resnet.py
If you use this implementation in you work, please don't forget to mention the
author, Yerlan Idelbayev.
'''
import torch.nn.init as init

def _weights_init(m):
    if isinstance(m, nn.Linear) or isinstance(m, nn.Conv2d):
        init.kaiming_normal_(m.weight)

class LambdaLayer(nn.Module):
    def __init__(self, lambd):
        super(LambdaLayer, self).__init__()
        self.lambd = lambd

    def forward(self, x):
        return self.lambd(x)


class SBasicBlock(nn.Module):
    expansion = 1

    def __init__(self, in_planes, planes, stride=1, actFunc='ReLU', intFunc='Conv', normFunc='BatchNorm', option='A'):
        super(SBasicBlock, self).__init__()
        self.conv1 = define_integration_function(intFunc, in_planes, planes, kernel_size=3, stride=stride, padding=1, bias=False)
        self.bn1 = define_norm_function(normFunc, planes)
        self.act1 = define_activation_function(actFunc, planes)
        self.conv2 = define_integration_function(intFunc, planes, planes, kernel_size=3, stride=1, padding=1, bias=False)
        self.bn2 = define_norm_function(normFunc, planes)

        self.shortcut = nn.Sequential()
        if stride != 1 or in_planes != planes:
            if option == 'A':
                """
                For CIFAR10 ResNet paper uses option A.
                """
                self.shortcut = LambdaLayer(lambda x:
                                            F.pad(x[:, :, ::2, ::2], (0, 0, 0, 0, planes//4, planes//4), "constant", 0))
            elif option == 'B':
                self.shortcut = nn.Sequential(
                     define_integration_function(intFunc, in_planes, self.expansion * planes, kernel_size=1, stride=stride, bias=False),
                     define_norm_function(normFunc, self.expansion * planes)
                )
        self.actOut=define_activation_function(actFunc, self.expansion*planes)

    def forward(self, x):
        out = self.act1(self.bn1(self.conv1(x)))
        out = self.bn2(self.conv2(out))
        out += self.shortcut(x)
        out = self.actOut(out)
        return out


class SResNet(nn.Module):
    def __init__(self, block, num_blocks, num_classes=10, inChannels=3, actFunc='ReLU', intFunc='Conv', normFunc='BatchNorm', inputNorm=None):
        super(SResNet, self).__init__()
        self.inputNorm=InputNormalisationLayer(inputNorm, inChannels)
        self.in_planes = 16
        self.conv1 = nn.Conv2d(inChannels, 16, kernel_size=3, stride=1, padding=1, bias=False)
        self.bn1 = nn.BatchNorm2d(16)
        self.layer1 = self._make_layer(block, 16, num_blocks[0], stride=1, actFunc=actFunc, intFunc=intFunc, normFunc=normFunc)
        self.layer2 = self._make_layer(block, 32, num_blocks[1], stride=2, actFunc=actFunc, intFunc=intFunc, normFunc=normFunc)
        self.layer3 = self._make_layer(block, 64, num_blocks[2], stride=2, actFunc=actFunc, intFunc=intFunc, normFunc=normFunc)
        self.linear = nn.Linear(64, num_classes)

        self.apply(_weights_init)

    def _make_layer(self, block, planes, num_blocks, stride, actFunc, intFunc, normFunc):
        strides = [stride] + [1]*(num_blocks-1)
        layers = []
        for stride in strides:
            layers.append(block(self.in_planes, planes, stride, actFunc, intFunc, normFunc))
            self.in_planes = planes * block.expansion

        return nn.Sequential(*layers)

    def forward(self, x):
        x = self.inputNorm(x)
        out = F.relu(self.bn1(self.conv1(x)))
        out = self.layer1(out)
        out = self.layer2(out)
        out = self.layer3(out)
        out = F.avg_pool2d(out, out.size()[3])
        out = out.view(out.size(0), -1)
        out = self.linear(out)
        return out


#############################################################################
# PreActResNet - code modified from https://github.com/P2333/Bag-of-Tricks-for-AT
#############################################################################
'''Pre-activation ResNet in PyTorch.

Reference:
[1] Kaiming He, Xiangyu Zhang, Shaoqing Ren, Jian Sun
    Identity Mappings in Deep Residual Networks. arXiv:1603.05027
'''

class PreActBlock(nn.Module):
    '''Pre-activation version of the BasicBlock.'''
    expansion = 1

    def __init__(self, in_planes, planes, stride=1, actFunc='ReLU', intFunc='Conv', normFunc='BatchNorm'):
        super(PreActBlock, self).__init__()
        self.bn1 = define_norm_function(normFunc, in_planes)
        self.act1 = define_activation_function(actFunc, in_planes)
        self.conv1 = define_integration_function(intFunc, in_planes, planes, kernel_size=3, stride=stride, padding=1, bias=False)
        self.bn2 = define_norm_function(normFunc, planes)
        self.act2 = define_activation_function(actFunc, planes)
        self.conv2 = define_integration_function(intFunc, planes, planes, kernel_size=3, stride=1, padding=1, bias=False)

        if stride != 1 or in_planes != self.expansion*planes:
            self.shortcut = nn.Sequential(
                define_integration_function(intFunc, in_planes, self.expansion*planes,
                          kernel_size=1, stride=stride, padding=0, bias=False),
            )

    def forward(self, x):
        out = self.act1(self.bn1(x))
        shortcut = self.shortcut(out) if hasattr(self, 'shortcut') else x
        out = self.conv1(out)
        out = self.conv2(self.act2(self.bn2(out)))
        out += shortcut
        return out


class PreActBottleneck(nn.Module):
    '''Pre-activation version of the original Bottleneck module.'''
    expansion = 4

    def __init__(self, in_planes, planes, stride=1, actFunc='ReLU', intFunc='Conv', normFunc='BatchNorm'):
        super(PreActBottleneck, self).__init__()
        self.bn1 = define_norm_function(normFunc, in_planes)
        self.act1 = define_activation_function(actFunc, in_planes)
        self.conv1 = define_integration_function(intFunc, in_planes, planes, kernel_size=1, stride=1, padding=0, bias=False)
        self.bn2 = define_norm_function(normFunc, planes)
        self.act2 = define_activation_function(actFunc, planes)
        self.conv2 = define_integration_function(intFunc, planes, planes, kernel_size=3, stride=stride, padding=1, bias=False)
        self.bn3 = define_norm_function(normFunc, planes)
        self.act3 = define_activation_function(actFunc, planes)
        self.conv3 = define_integration_function(intFunc, planes, self.expansion*planes, kernel_size=1, stride=1, padding=0, bias=False)

        if stride != 1 or in_planes != self.expansion*planes:
            self.shortcut = nn.Sequential(
                define_integration_function(intFunc, in_planes, self.expansion*planes,
                          kernel_size=1, stride=stride, padding=0, bias=False),
            )

    def forward(self, x):
        out = self.act1(self.bn1(x))
        shortcut = self.shortcut(out) if hasattr(self, 'shortcut') else x
        out = self.conv1(out)
        out = self.conv2(self.act2(self.bn2(out)))
        out = self.conv3(self.act3(self.bn3(out)))
        out += shortcut
        return out


class PreActResNet(nn.Module):
    def __init__(self, block, num_blocks, num_classes=10, inChannels=3, actFunc='ReLU', intFunc='Conv', normFunc='BatchNorm', inputNorm=None, normalize = False, normalize_only_FN = False, scale = 15):
        super(PreActResNet, self).__init__()
        self.inputNorm=InputNormalisationLayer(inputNorm, inChannels)
        self.in_planes = 64
        self.normalize = normalize
        self.normalize_only_FN = normalize_only_FN
        self.scale = scale

        self.conv1 = nn.Conv2d(inChannels, 64, kernel_size=3, stride=1, padding=1, bias=False)
        self.layer1 = self._make_layer(block, 64, num_blocks[0], stride=1, actFunc=actFunc, intFunc=intFunc, normFunc=normFunc)
        self.layer2 = self._make_layer(block, 128, num_blocks[1], stride=2, actFunc=actFunc, intFunc=intFunc, normFunc=normFunc)
        self.layer3 = self._make_layer(block, 256, num_blocks[2], stride=2, actFunc=actFunc, intFunc=intFunc, normFunc=normFunc)
        self.layer4 = self._make_layer(block, 512, num_blocks[3], stride=2, actFunc=actFunc, intFunc=intFunc, normFunc=normFunc)
        self.bn = define_norm_function(normFunc, 512 * block.expansion)
        self.act = define_activation_function(actFunc, 512 * block.expansion)

        if self.normalize:
            self.linear = nn.Linear(512*block.expansion, num_classes, bias=False)
        else:
            self.linear = nn.Linear(512*block.expansion, num_classes)


    def _make_layer(self, block, planes, num_blocks, stride, actFunc, intFunc, normFunc):
        strides = [stride] + [1]*(num_blocks-1)
        layers = []
        for stride in strides:
            layers.append(block(self.in_planes, planes, stride, actFunc, intFunc, normFunc))
            self.in_planes = planes * block.expansion
        return nn.Sequential(*layers)

    def forward(self, x):
        x = self.inputNorm(x)
        out = self.conv1(x)
        out = self.layer1(out)
        out = self.layer2(out)
        out = self.layer3(out)
        out = self.layer4(out)
        out = self.act(self.bn(out))
        out = F.avg_pool2d(out, 4)
        out = out.view(out.size(0), -1)
        if self.normalize_only_FN:
            out = F.normalize(out, p=2, dim=1)

        if self.normalize:
            out = F.normalize(out, p=2, dim=1) * self.scale
            for _, module in self.linear.named_modules():
                if isinstance(module, nn.Linear):
                    module.weight.data = F.normalize(module.weight, p=2, dim=1)
        out=self.linear(out)
        return out


#############################################################################
# WideResNet - code modified from https://github.com/P2333/Bag-of-Tricks-for-AT
#############################################################################
import math
class WideBasicBlock(nn.Module):
    def __init__(self, in_planes, out_planes, stride, dropRate=0.0, actFunc='ReLU', intFunc='Conv', normFunc='BatchNorm'):
        super(WideBasicBlock, self).__init__()
        self.bn1 = define_norm_function(normFunc, in_planes)
        self.act1 = define_activation_function(actFunc, in_planes)
        self.conv1 = define_integration_function(intFunc, in_planes, out_planes, kernel_size=3, stride=stride, padding=1, bias=False)
        self.bn2 = define_norm_function(normFunc, out_planes)
        self.act2 = define_activation_function(actFunc, out_planes)
        self.conv2 = define_integration_function(intFunc, out_planes, out_planes, kernel_size=3, stride=1, padding=1, bias=False)
        self.droprate = dropRate
        self.equalInOut = (in_planes == out_planes)
        self.convShortcut = (not self.equalInOut) and nn.Conv2d(in_planes, out_planes, kernel_size=1, stride=stride,
                                                                padding=0, bias=False) or None

    def forward(self, x):
        if not self.equalInOut:
            x = self.act1(self.bn1(x))
        else:
            out = self.act1(self.bn1(x))
        out = self.act2(self.bn2(self.conv1(out if self.equalInOut else x)))
        if self.droprate > 0:
            out = F.dropout(out, p=self.droprate, training=self.training)
        out = self.conv2(out)
        return torch.add(x if self.equalInOut else self.convShortcut(x), out)


class NetworkBlock(nn.Module):
    def __init__(self, nb_layers, in_planes, out_planes, block, stride, dropRate=0.0, actFunc='ReLU', intFunc='Conv', normFunc='BatchNorm'):
        super(NetworkBlock, self).__init__()
        self.layer = self._make_layer(block, in_planes, out_planes, nb_layers, stride, dropRate, actFunc, intFunc, normFunc)

    def _make_layer(self, block, in_planes, out_planes, nb_layers, stride, dropRate, actFunc='ReLU', intFunc='Conv', normFunc='BatchNorm'):
        layers = []
        for i in range(int(nb_layers)):
            layers.append(block(i == 0 and in_planes or out_planes, out_planes, i == 0 and stride or 1, dropRate, actFunc, intFunc, normFunc))
        return nn.Sequential(*layers)

    def forward(self, x):
        return self.layer(x)


class WideResNet(nn.Module):
    def __init__(self, depth=34, num_classes=10, inChannels=3, widen_factor=10, dropRate=0.0, actFunc='ReLU', intFunc='Conv', normFunc='BatchNorm', inputNorm=None, normalize=False):
        super(WideResNet, self).__init__()
        self.inputNorm=InputNormalisationLayer(inputNorm, inChannels)
        nChannels = [16, 16 * widen_factor, 32 * widen_factor, 64 * widen_factor]
        assert ((depth - 4) % 6 == 0)
        n = (depth - 4) / 6
        block = WideBasicBlock
        self.normalize = normalize
        #self.scale = scale
        # 1st conv before any network block
        self.conv1 = nn.Conv2d(inChannels, nChannels[0], kernel_size=3, stride=1,
                               padding=1, bias=False)
        # 1st block
        self.block1 = NetworkBlock(n, nChannels[0], nChannels[1], block, 1, dropRate, actFunc, intFunc, normFunc)
        # 1st sub-block
        self.sub_block1 = NetworkBlock(n, nChannels[0], nChannels[1], block, 1, dropRate, actFunc, intFunc, normFunc)
        # 2nd block
        self.block2 = NetworkBlock(n, nChannels[1], nChannels[2], block, 2, dropRate, actFunc, intFunc, normFunc)
        # 3rd block
        self.block3 = NetworkBlock(n, nChannels[2], nChannels[3], block, 2, dropRate, actFunc, intFunc, normFunc)
        # global average pooling and classifier
        self.bn1 = define_norm_function(normFunc, nChannels[3])
        self.act = define_activation_function(actFunc, nChannels[3])

        if self.normalize:
            self.fc = nn.Linear(nChannels[3], num_classes, bias = False)
        else:
            self.fc = nn.Linear(nChannels[3], num_classes)
        self.nChannels = nChannels[3]

        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                n = m.kernel_size[0] * m.kernel_size[1] * m.out_channels
                m.weight.data.normal_(0, math.sqrt(2. / n))
            elif isinstance(m, nn.BatchNorm2d):
                m.weight.data.fill_(1)
                m.bias.data.zero_()
            elif isinstance(m, nn.Linear) and not self.normalize:
                m.bias.data.zero_()

    def forward(self, x):
        x = self.inputNorm(x)
        out = self.conv1(x)
        out = self.block1(out)
        out = self.block2(out)
        out = self.block3(out)
        out = self.act(self.bn1(out))
        out = F.avg_pool2d(out, 8)
        out = out.view(-1, self.nChannels)
        if self.normalize:
            out = F.normalize(out, p=2, dim=1)
            for _, module in self.fc.named_modules():
                if isinstance(module, nn.Linear):
                    module.weight.data = F.normalize(module.weight, p=2, dim=1)
        out=self.fc(out)
        return out

