#############################################################################
# Code to record the output produced by a model to 
# clean, corrupt, adversarial, unknown and unrecognisable test images.
# This output is saved for subsequent analysis
#
# (c) 2023 Michael William Spratling
#############################################################################

import torch
import torchattacks
from autoattack import AutoAttack
import load_dataset
from utils import plot_images, import_weights
device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")

#############################################################################
# define experiment to perform
#############################################################################
from expt_def import expt_def
netType, task, dataOOD, dataAug, actFunc, intFunc, normFunc, poolFunc, lossFunc, balanceMethod, fileName=expt_def()
batchSize=200 #define number of images in each test set batch
showExemplars=False
dataTypes=['clean','corrupt','adversarial','unknown','unrecog'] #'fooling']
adversaryTypes=['Linf','L2']

if task.startswith('MNIST'):
    corruptions=load_dataset.corruptionsMNIST
    epsilonLinf=0.3
    epsilonL2=2
    unknown=['Omniglot','FashionMNIST','KMNIST']
elif task.startswith('CIFAR100'):
    corruptions=load_dataset.corruptionsCIFAR
    epsilonLinf=8/255
    epsilonL2=0.5
    unknown=['Textures','SVHN','CIFAR10']
elif task.startswith('CIFAR10'):
    corruptions=load_dataset.corruptionsCIFAR
    epsilonLinf=8/255
    epsilonL2=0.5
    unknown=['Textures','SVHN','CIFAR100']
elif task.startswith('TinyImageNet'):
    corruptions=load_dataset.corruptionsTinyImageNet
    epsilonLinf=8/255
    epsilonL2=0.5
    unknown=['Textures','iNaturalist','ImageNetO']
elif task.startswith('ImageNet'): # WARNING: datasets sizes not well balanced, evaluation will be inaccurate/biased
    corruptions=load_dataset.corruptionsImageNet
    epsilonLinf=8/255
    epsilonL2=0.5
    unknown=['Textures','iNaturalist','ImageNetO']
    batchSize=20
unrecog=['AugPhase','AugScramble','SynthBlobs','SynthUniform']
from load_dataset import load_dataset
_, _, imDims, numClasses, _, _ = load_dataset(task,batchSize)

#############################################################################
# define the NN architecture to be tested
#############################################################################
#allow choice of model to be assessed, made by the arguments in "expt_def.py", 
#to be overwritten by a filename inputted as a command-line option to this function
import argparse
from model_defs import import_model, define_model
ap = argparse.ArgumentParser()
ap.add_argument("-f", "--file", required=False, help="file to process (over-riding expt_def)")
args = vars(ap.parse_args())
if args['file']!=None:
    fileName=args['file']
    model = import_model(netType, actFunc, numClasses, device, fileName)
else:
    model = define_model(netType, actFunc, intFunc, normFunc, poolFunc, imDims, numClasses, task, device)
    #model.load_state_dict(torch.load(fileName))
    model = import_weights(fileName, model, device)

model.eval() # prepare model for testing

#############################################################################
# find output of model for all datasets
#############################################################################
def record_predictions(test_loader, model, yPred, classGT, device='cpu'):
    #for each sample find the output produced by the network
    
    numSamples=0
    numCorrect=0
    # initialize variables to record predictions
    for samples, labels in test_loader:
        samples, labels = samples.to(device), labels.to(device)
        y = model(samples).detach()
        _, classPred = torch.max(y, 1)
        numCorrect+=torch.sum(classPred==labels)
        #append results to existing ones and those for previous batches
        yPred=torch.cat((yPred,y),dim=0)
        classGT=torch.cat((classGT,labels))
        numSamples+=len(labels)

    acc=100.0*numCorrect/numSamples
    print('num samples in dataset=',numSamples,'accuaracy (without rejection)= %.2f %%' % acc,flush=True)
    return yPred, classGT, numSamples


#define variables to record model's output
yPred=torch.tensor([]).to(device)
classGT=torch.tensor([]).to(device)
acceptGT=torch.tensor([]).to(device)
dataType=torch.tensor([]).to(device)

# Test model on clean data
_, test_loader, imDims, numClasses, _, _ = load_dataset(task,batchSize,False,showExemplars)
imDimsReqd=list(imDims)
#determine and record model's predictions
yPred, classGT, numforDataset=record_predictions(test_loader, model, yPred, classGT, device=device)
tmpOnes=torch.ones(numforDataset).to(device)
acceptGT=torch.cat((acceptGT,tmpOnes))
dataType=torch.cat((dataType,tmpOnes*dataTypes.index('clean')))


# Test model on corrupt data
for corruption in corruptions:
    print("Testing with",corruption)
    # load dataset
    if task.startswith('MNIST'):
        _, test_loader, _, _, _, _ = load_dataset('MNISTC',batchSize,False,showExemplars,None,corruption)
    elif task.startswith('CIFAR100'):
        _, test_loader, _, _, _, _ = load_dataset('CIFAR100C',batchSize,False,showExemplars,None,corruption)
    elif task.startswith('CIFAR10'):
        _, test_loader, _, _, _, _ = load_dataset('CIFAR10C',batchSize,False,showExemplars,None,corruption)
    elif task.startswith('TinyImageNet'):
        _, test_loader, _, _, _, _ = load_dataset('TinyImageNetC',batchSize,False,showExemplars,None,corruption)
    elif task.startswith('ImageNet'):
        _, test_loader, _, _, _, _ = load_dataset('ImageNetC',batchSize,False,showExemplars,None,corruption)
    #determine and record model's predictions
    yPred, classGT, numforDataset=record_predictions(test_loader, model, yPred, classGT, device=device)
    tmpOnes=torch.ones(numforDataset).to(device)
    acceptGT=torch.cat((acceptGT,tmpOnes))
    dataType=torch.cat((dataType,tmpOnes*dataTypes.index('corrupt')))


# Test model on adversarial data
# obtain images that will be attacked
_, test_loader, _, _, _, _ = load_dataset(task,batchSize,False,False)
for advType in adversaryTypes:
    if advType=='Linf':
        epsilon=epsilonLinf
    elif advType=='L2':
        epsilon=epsilonL2
    print("\nTesting with",advType,"AutoAttack using epsilon=",epsilon,"\n",flush=True)
    attack = torchattacks.AutoAttack(model, norm=advType, eps=epsilon, version='standard', n_classes=numClasses) #torchattacks version
    #attack = AutoAttack(model, norm=advType, eps=epsilon) #autoattack version
    #attack images in each back, and gather results together
    allLabels=torch.tensor([]).to(device)
    allImages=torch.tensor([]).to(device)
    for samples, labels in test_loader:
        samples, labels = samples.to(device), labels.to(device)
        advImagesBatch = attack(samples, labels) #torchattacks version
        #advImagesBatch = attack.run_standard_evaluation(samples, labels) #autoattack version
        #if advType=='Linf': print(torch.max(samples-advImagesBatch))
        #if advType=='L2': print(torch.max(torch.sqrt(torch.sum((samples-advImagesBatch)**2,dim=(1,2,3)))))
        allLabels=torch.cat((allLabels,labels))
        allImages=torch.cat((allImages,advImagesBatch.detach()),0)
    allLabels=allLabels.type(torch.LongTensor)
    allImages, allLabels = allImages.to(device), allLabels.to(device)
    if showExemplars:
        plot_images(allImages, allLabels)
    #determine and record model's predictions
    adv_dataset=torch.utils.data.TensorDataset(allImages,allLabels)
    adv_loader = torch.utils.data.DataLoader(adv_dataset, shuffle=False, batch_size=batchSize)
    yPred, classGT, numforDataset=record_predictions(adv_loader, model, yPred, classGT, device=device)
    tmpOnes=torch.ones(numforDataset).to(device)
    acceptGT=torch.cat((acceptGT,tmpOnes))
    dataType=torch.cat((dataType,tmpOnes*dataTypes.index('adversarial')))


# Test model on unknown data
for t in unknown:
    print("Testing with",t)
    #load dataset
    _, test_loader, _, _, _, _ = load_dataset(t,batchSize,False,showExemplars,imDimsReqd=imDimsReqd)
    #determine and record model's predictions
    yPred, classGT, numforDataset=record_predictions(test_loader, model, yPred, classGT, device=device)
    tmpOnes=torch.ones(numforDataset).to(device)
    acceptGT=torch.cat((acceptGT,torch.zeros(numforDataset).to(device)))
    dataType=torch.cat((dataType,tmpOnes*dataTypes.index('unknown')))


# Test model on unrecognisable/random synthetic images
from augment_data import random_fourier_phase, scramble_pixels
for t in unrecog:
    print("Testing with",t)
    #load dataset
    if t.startswith('Aug'):
        #load clean test data ready for modification
        _, test_loader, _, _, _, _ = load_dataset(task,batchSize,False,False)
        #apply augmentation to all samples
        imagesAug=torch.tensor([]).to(device)
        labelsAug=torch.tensor([]).to(device)
        for images, labels in test_loader:
            images, labels = images.to(device), labels.to(device)
            if t=='AugPhase':
                images=random_fourier_phase(images)
            elif t=='AugScramble':
                images=scramble_pixels(images)
            imagesAug=torch.cat((imagesAug,images.detach()),0)
            labelsAug=torch.cat((labelsAug,labels),0)
        if showExemplars:
            plot_images(images)
        #replace data loader with one containing the modified images
        aug_dataset=torch.utils.data.TensorDataset(imagesAug,labelsAug)
        test_loader = torch.utils.data.DataLoader(aug_dataset, shuffle=False, batch_size=batchSize)
    else:
        #load dataset from storage
        _, test_loader, _, _, _, _ = load_dataset(t,batchSize,False,showExemplars,imDimsReqd=imDimsReqd)
    
    #determine and record model's predictions
    yPred, classGT, numforDataset=record_predictions(test_loader, model, yPred, classGT, device=device)
    tmpOnes=torch.ones(numforDataset).to(device)
    acceptGT=torch.cat((acceptGT,torch.zeros(numforDataset).to(device)))
    dataType=torch.cat((dataType,tmpOnes*dataTypes.index('unrecog')))

# Test model on fooling images
# for t in fooling:
#     print("\nTesting with fooling images based on",t)
#     #load dataset that forms the starting images from which fooling images are created
#     _, test_loader, _, _, _, _ = load_dataset(t,batchSize,False,False,imDimsReqd=imDimsReqd)
#     # gather images that will be attacked and give false labels to those images so that they 
#     # are all classified correctly, and hence, will all be attacked
#     falseLabels=torch.tensor([]).to(device)
#     images=torch.tensor([]).to(device)
#     for samples, labels in test_loader:
#         samples, labels = samples.to(device), labels.to(device)
#         # forward pass: compute predicted outputs by passing inputs to the model
#         _, falseLabelsBatch = torch.max(model(samples).detach(), 1)
#         falseLabels=torch.cat((falseLabels,falseLabelsBatch))
#         images=torch.cat((images,samples),0)
#     labels=falseLabels.type(torch.LongTensor)
#     images, labels = images.to(device), labels.to(device)
#     # use AutoAttack to change images so that they are more similar to IID samples
#     for advType in adversaryTypes:
#         if advType=='Linf':
#             print("and corrupted with Linf AutoAttacks using epsilon=",epsilonLinf,"\n",flush=True)
#             adversary = AutoAttack(model, norm='Linf', eps=epsilonLinf, version='standard', device=device)
#         elif advType=='L2':
#             print("and corrupted with L2 AutoAttacks using epsilon=",epsilonL2,"\n",flush=True)
#             adversary = AutoAttack(model, norm='L2', eps=epsilonL2, version='standard', device=device)
#         foolingImages = adversary.run_standard_evaluation(images, labels, bs=batchSize)
#         if showExemplars:
#             plot_images(images)
#         #determine and record model's predictions
#         fool_dataset=torch.utils.data.TensorDataset(foolingImages, labels)
#         fool_loader = torch.utils.data.DataLoader(fool_dataset, shuffle=False, batch_size=batchSize)
#         yPred, classGT, numforDataset=record_predictions(fool_loader, model, yPred, classGT, device=device)
#         tmpOnes=torch.ones(numforDataset).to(device)    
#         acceptGT=torch.cat((acceptGT,torch.zeros(numforDataset).to(device)))
#         dataType=torch.cat((dataType,tmpOnes*dataTypes.index('fooling')))


#############################################################################
# save results
#############################################################################
results={
    'yPred': yPred,
    'classGT': classGT,
    'acceptGT': acceptGT,
    'dataType': dataType,
    'dataTypes': dataTypes
}
fileName=fileName+'_outputs'
torch.save(results, fileName)

