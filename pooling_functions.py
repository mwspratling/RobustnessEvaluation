#############################################################################
# Code defining pooling functions
#
# (c) 2023 Michael William Spratling
#############################################################################
import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
from utils import parse_string

def define_pooling_function(poolFunc, poolSize):
    #convert string defining name of function to a definition for a neural net module
    if poolFunc=='AvgPool':
        poolFunc=nn.AvgPool2d(poolSize)
    elif poolFunc=='MaxPool':
        poolFunc=nn.MaxPool2d(poolSize)
    elif poolFunc.startswith('LPPool'):
        normVal=parse_string(poolFunc,'LPPool','/')
        normVal=float(normVal[0])
        poolFunc=nn.LPPool2d(normVal,poolSize)
    elif poolFunc=='MinPool':
        poolFunc=MinPool2d(poolSize)
    else:
        print("ERROR: undefined pooling function")
    return poolFunc

class MinPool2d(nn.Module):
    # a custom pooling function that performs min_pooling
    def __init__(self, poolSize):
        super(MinPool2d,self).__init__()
        self.poolSize=poolSize
        
    def forward(self, x):
        y = -F.max_pool2d(-x, self.poolSize)
        return y


