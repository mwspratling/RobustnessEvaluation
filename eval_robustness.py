#############################################################################
# Code to test a model's ability to classify IID/corrupt and adversarial test images, 
# while also being able to reject test images from unknown classes and 
# any other image for which predicted class is wrong.
# Performance is evaluated using percent detection accuracy rate (DAR).
#
# (c) 2023 Michael William Spratling
#############################################################################

import numpy as np
import torch
import torch.nn.functional as F
import argparse
import sys
from utils import pasteable_array, print_stats
device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")

#############################################################################
# define experiment to perform
#############################################################################
from expt_def import expt_def
netType, task, dataOOD, dataAug, actFunc, intFunc, normFunc, poolFunc, lossFunc, balanceMethod, fileName=expt_def()
rejectMethod='MSP' # 'Sep' # 'GEN' # 'Energy' # 'MLS' # 

#############################################################################
# load file containing information about model's output for different datasets
#############################################################################
#allow choice of data to be assessed, made by the arguments in "expt_def.py", 
#to be overwritten by a filename inputted as a command-line option to this function
ap = argparse.ArgumentParser()
ap.add_argument("-f", "--file", required=False, help="file to process (over-riding expt_def)")
args = vars(ap.parse_args())
if args['file']!=None:
    fileName=args['file']
    #allow file name that is input to either end in .py or .py_outputs
    fileName=fileName.split('_outputs')
    fileName=fileName[0]
    
fileName=fileName+'_outputs' # this file is produced by "eval_model_all_data.py"
print('assessing data in file:',fileName)
result = torch.load(fileName,map_location=device)
yPred = result['yPred']
classGT = result['classGT']
acceptGT = result['acceptGT']
dataType = result['dataType']
dataTypes = result['dataTypes']

if yPred.isnan().any():
    sys.exit("unable to evaluate robustness of network that has returned nan activation values")

#############################################################################
# find model's predictions for all datasets
#############################################################################
#calculate model's prediction of class label, and 
#confidence that prediction should be accepted
#confidence measured using:
#MLS: the maximum response of the network measured from the logits
#MSP: the maximum response of the network measured from the softmax output
#Energy: the log of the sum of exp(y)
#GEN: method from https://github.com/XixiLiu95/GEN
#Sep: difference between the maximum and second highest responses
if rejectMethod=='MSP' or rejectMethod=='GEN':
    yPred=F.softmax(yPred, dim=1)
    
acceptPred, classPred = torch.max(yPred, 1)

if rejectMethod=='Energy':
    tempVal=1.0
    acceptPred = tempVal * torch.logsumexp(yPred/tempVal, dim=1)
    #acceptPred = tempVal*torch.log( torch.sum( torch.exp(yPred)/tempVal, dim=1)) #equivalent, but unstable!

if rejectMethod=='GEN':
    gamma=0.1
    M=100 #int(0.1*yPred.size(1)) # 0.1*numClasses - v.poor with 10 classes!
    probsSorted = torch.sort(yPred, dim=1).values
    probsSorted = probsSorted[:,-M:] #take top-M probs
    acceptPred = -torch.sum(probsSorted**gamma * (1 - probsSorted)**(gamma), dim=1)
   
if rejectMethod=='Sep':
    #subtract second highest response
    ySort=torch.sort(yPred, dim=1, descending=True).values
    #acceptPred-=ySort[:,1]
    acceptPred-=torch.mean(ySort[:,1:], dim=1)
    #acceptPred-=torch.mean(F.relu(ySort[:,1:]), dim=1)
    #acceptPred-=torch.sum(ySort[:,1:]/(torch.arange(1,ySort.size(1))).to(ySort.device), dim=1)

    #excess=F.relu(0.2+ySort[:,1:]-acceptPred[:,None])
    #weigh loss towards least correct responses - improves learning especially when number of classes is large
    #excess=excess/(torch.arange(1,ySort.size(1))).to(ySort.device)
    #acceptPred-=torch.sum(excess, dim=1)



#############################################################################
# assess model's predictions
#############################################################################
def calc_zhu_DER_by_type(classPred,acceptPred,classGT,acceptGT,acceptThres,dataType,dataTypes,device='cpu',printResults=False):
    #In contrast to original method this function returns the accuracy not the error
    scoreByType=torch.zeros(len(dataTypes)+1)
    scoreByType[0]=acceptThres
    idxCorrect=torch.zeros_like(classPred)
    for type in np.arange(len(dataTypes)):
        idxType=type==dataType
        #for each type of data calculate its score for this threshold
        
        #for samples that are accepted by the model, 
        indAccept=torch.logical_and(acceptPred>=acceptThres,idxType)
        #prediction is TP if
        #1. sample is from known class (acceptGT=1), AND
        #2. sample is correctly classified (classPred=classGT)
        cond1=torch.logical_and(indAccept,acceptGT==1)
        cond2=torch.logical_and(indAccept,classPred==classGT)
        correctAccept=torch.logical_and(cond1,cond2)
        TP=torch.sum(correctAccept)
        #prediction is FP for all other accepted samples
        FP=torch.sum(indAccept)-TP

        #for samples that are rejected by the model, 
        indReject=torch.logical_and(acceptPred<acceptThres,idxType)
        #prediction is TN if
        #1. sample is from unknown class (acceptGT=0), OR
        #2. sample is incorrectly classified (classPred!=classGT)
        cond1=torch.logical_and(indReject,acceptGT==0)
        cond2=torch.logical_and(indReject,classPred!=classGT)
        correctReject=torch.logical_or(cond1,cond2)
        TN=torch.sum(correctReject)
        #prediction is FN for all other rejected samples
        FN=torch.sum(indReject)-TN
    
        #samples that are correctly dealt with by model
        idxCorrect=torch.logical_or(idxCorrect,correctAccept)
        idxCorrect=torch.logical_or(idxCorrect,correctReject)

        DER=(FP+FN)/(TP+TN+FP+FN+1e-9)
        score=100*(1-DER)
        scoreByType[type+1]=score
        if printResults:
            print('Accuracy for data type %11s: %3.2f %% (TP=%d, TN=%d, FP=%d, FN=%d)' % (
                dataTypes[type], scoreByType[type+1], TP, TN, FP, FN))
        if dataTypes[type]=='clean':
            iidTPR=(TP/(TP+FN)).to("cpu")

    if printResults: pasteable_array(scoreByType)
    
    accuracyCombined=torch.mean(scoreByType[1:])
    if printResults: print('Overall Robustness (based on method of Zhu et al): %.2f %%\n' % accuracyCombined,flush=True)

    return accuracyCombined, idxCorrect, iidTPR

# def calc_alt_score_by_type(classPred,acceptPred,classGT,acceptGT,acceptThres,dataType,dataTypes,device='cpu',printResults=False):
#     scoreByType=torch.zeros(len(dataTypes)+1)
#     scoreByType[0]=acceptThres
#     idxCorrect=torch.zeros_like(classPred)
#     for type in np.arange(len(dataTypes)):
#         idxType=type==dataType
#         #for each type of data calculate its score for this threshold
        
#         #for samples that are accepted by the model, 
#         indAccept=torch.logical_and(acceptPred>acceptThres,idxType)
#         #prediction is TP if
#         #1. sample is from known class (acceptGT=1), AND
#         #2. sample is correctly classified (classPred=classGT)
#         cond1=torch.logical_and(indAccept,acceptGT==1)
#         cond2=torch.logical_and(indAccept,classPred==classGT)
#         correctAccept=torch.logical_and(cond1,cond2)
#         TP=torch.sum(correctAccept)
#         #prediction is FP for all other accepted samples
#         FP=torch.sum(indAccept)-TP
        
#         #for samples that are rejected by the model, 
#         indReject=torch.logical_and(acceptPred<=acceptThres,idxType)
#         #prediction is TN if
#         #1. sample is from unknown class (acceptGT=0), 
#         correctReject=torch.logical_and(indReject,acceptGT==0)
#         TN=torch.sum(correctReject)
#         #prediction is FN if
#         #1. sample is from known class (acceptGT=1), AND
#         #2. sample is correctly classified (classPred=classGT)
#         cond1=torch.logical_and(indReject,acceptGT==1)
#         cond2=torch.logical_and(indReject,classPred==classGT)
#         wrongReject=torch.logical_and(cond1,cond2)
#         FN=torch.sum(wrongReject)
                
#         #samples that are correctly dealt with by model
#         idxCorrect=torch.logical_or(idxCorrect,correctAccept)
#         idxCorrect=torch.logical_or(idxCorrect,correctReject)

#         accuracy=(TP+TN)/(TP+TN+FP+FN+1e-9)      
#         accuracy=100*accuracy
#         scoreByType[type+1]=accuracy
#         if printResults:
#             print('Accuracy (proposed method) for data type %11s: %3.2f %% (TP=%d, TN=%d, FP=%d, FN=%d)' % (
#                 dataTypes[type], scoreByType[type+1], TP, TN, FP, FN))
#         if dataTypes[type]=='clean':
#             accuracyIID=accuracy.to("cpu")

#     if printResults: pasteable_array(scoreByType)
    
#     accuracyCombined=torch.mean(scoreByType[1:])
#     if printResults: print('Overall Robustness (proposed method): %.2f %%\n' % accuracyCombined,flush=True)

#     return accuracyCombined, idxCorrect, accuracyIID

import matplotlib.pyplot as plt
def plot_results(classPred,acceptPred,classGT,acceptGT,acceptThres,dataType,dataTypes,idxCorrect):
    plt.figure()
    acceptPred=acceptPred.to(torch.device("cpu"))
    step=(acceptPred.max()-acceptPred.min())/20.0
    bins=np.arange(start=acceptPred.min(),stop=acceptPred.max()+step,step=step)
    for type in np.arange(len(dataTypes)):
        histData=np.array(acceptPred)[np.array((type==dataType).to(torch.device("cpu")))]
        plt.subplot(2, 3, type+1)
        plt.hist(histData, bins = bins)
        plt.title('confidence for %s data' % dataTypes[type])

    for type in np.arange(len(dataTypes)):
        histDataCorrect=np.array(acceptPred)[np.array(torch.logical_and(idxCorrect,type==dataType).to(torch.device("cpu")))]
        histDataWrong=np.array(acceptPred)[np.array(torch.logical_and(~idxCorrect,type==dataType).to(torch.device("cpu")))]
        plt.figure(figsize=(5, 3))
        plt.subplot(2, 1, 1)
        plt.hist(histDataCorrect, bins = bins)
        plt.xticks([])
        plt.subplot(2, 1, 2)
        plt.hist(histDataWrong, bins = bins)
        ax = plt.gca()
        ax.invert_yaxis()
        plt.title('confidence for correct/wrongly classified %s data' % dataTypes[type])

def plot_score_vs_thres(thresholds,scores):
    plt.figure()
    plt.plot(thresholds.to(torch.device("cpu")), scores)

#############################################################################
# report the traditional, individual, metrics for some types of data
#############################################################################
print('========================================================================================')
print('Results with no-rejection = Standard accuracy metric for data of type clean, corrupt and adversarial')
acceptThres=torch.tensor([-1e10]).to(device)
calc_zhu_DER_by_type(classPred,acceptPred,classGT,acceptGT,acceptThres,dataType,dataTypes,device=device,printResults=True)
print('----------------------------------------------------------------------------------------')

#############################################################################
# report results calculated with method described in arXiv:2211.16778) using
# threshold set using clean data
#############################################################################
print('Using unknown class rejection method',rejectMethod)
print('Assessing performance (using method proposed by Zhu et al) with acceptance threshold set in different ways\n')

#find and sort acceptance confidance for all correctly classified clean samples
for type in np.arange(len(dataTypes)):
    if dataTypes[type]=='clean':
        break
idxIID=torch.logical_and(classPred==classGT,type==dataType) #correctly classified clean samples
#idxIID=(type==dataType) #clean samples
acceptPredIID=acceptPred[idxIID]
acceptPredIID=torch.sort(acceptPredIID).values

#evaluate performance with threshold based on mean confidence of correctly classified clean samples
#for std in [0,1]:
#    acceptThres=torch.mean(acceptPredIID)-torch.std(acceptPredIID)*std
#    print('Results with threshold equal to mean-%.1fstd of confidence for correctly classified clean samples' % std)
#    accuracy, idxCorrect, _ = calc_zhu_DER_by_type(classPred,acceptPred,classGT,acceptGT,acceptThres,dataType,dataTypes,device=device,printResults=True)
#    #plot_results(classPred,acceptPred,classGT,acceptGT,acceptThres,dataType,dataTypes,idxCorrect)

#evaluate performance with threshold set so that a fixed proportion of correctly classified clean samples are accepted
for proportion in [0.99,0.95]:
    acceptThres=acceptPredIID[int((1.0-proportion)*len(acceptPredIID))]
    print('Results with %.1f%% of correctly classified clean samples accepted' % (100.0*proportion))
    accuracy, idxCorrect, _ = calc_zhu_DER_by_type(classPred,acceptPred,classGT,acceptGT,acceptThres,dataType,dataTypes,device=device,printResults=True)
    #plot_results(classPred,acceptPred,classGT,acceptGT,acceptThres,dataType,dataTypes,idxCorrect)
