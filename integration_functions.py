#############################################################################
# Code defining integration functions
#
# (c) 2023 Michael William Spratling
#############################################################################

import torch
import torch.nn as nn
import torch.nn.functional as F

def define_integration_function(intFunc, inChannels, outChannels, kernel_size, stride, padding, bias, layerDims=None):
    #convert string defining name of function to a definition for a neural net module
    if intFunc=='Conv':
        intFunc=nn.Conv2d(inChannels, outChannels, kernel_size, stride, padding, bias=bias)
    else:
        print("ERROR: undefined integration function")
    return intFunc
