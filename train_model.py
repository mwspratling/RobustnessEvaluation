#############################################################################
# Code to train a neural network
#
# (c) 2023 Michael William Spratling
#############################################################################

import torch
import torchattacks
import time
import sys
import os
import numpy as np
from utils import parse_string, print_stats, print_model_details, test_network_accuracy, plot_conv_weights
device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")

#############################################################################
# define experiment to perform
#############################################################################
from expt_def import expt_def
netType, task, dataOOD, dataAug, actFunc, intFunc, normFunc, poolFunc, lossFunc, balanceMethod, fileName=expt_def()

intermittentAssessment=False
normInputs=False
#############################################################################
# load data set
#############################################################################
batchSize=128
#if 'Consist' in lossFunc: batchSize=int(batchSize/2)
if task.startswith('CIFAR') or task.startswith('TinyImageNet'):
    augmentData=True
else:
    augmentData=False
from load_dataset import load_dataset

train_loader, test_loader, imDims, numClasses, classNames, samplesPerClass = load_dataset(task,batchSize,augmentData)

#############################################################################
# define additional OOD data (if used)
#############################################################################
if dataOOD=='EMNISTletters' or dataOOD=='300KRandomImages':
    ood_loader, _, _, _, _, _ = load_dataset(dataOOD,batchSize,augmentData)
    
#############################################################################
# define the NN architecture to be trained
#############################################################################
#if required add input normalisation to model 
#usual for OOD rejection, not used for adversarial robustness
inputNormValues = {
    'MNIST': [[0.1307], [0.3081]],
    'CIFAR10': [[0.4914, 0.4822, 0.4465],[0.2470, 0.2435, 0.2616]],
    'CIFAR100': [[0.5071, 0.4867, 0.4408],[0.2675, 0.2565, 0.2761]],
    'ImageNet': [[0.485, 0.456, 0.406],[0.229, 0.224, 0.225]],
}
if normInputs:
    inputNorm=inputNormValues[task]
else:
    inputNorm=None

from model_defs import define_model
model = define_model(netType, actFunc, intFunc, normFunc, poolFunc, imDims, numClasses, task, device, inputNorm=inputNorm)

#show some information about model
params=print_model_details(model)
#Start calculation of an approximate computational cost that will be used to decide
#if training can complete within the max runtime allowed by the server, and hence,
#to decide if checkpoints should be saved during training.
#The value of compCost at which checkpoints are saved is obviously going to depend
#on the hardware used.
compCost=params*len(train_loader.dataset)/250e12

#############################################################################
# define the training settings
#############################################################################
if task.startswith('CIFAR') and '-LT' in task: 
    #training set-up used for learning with unbalanced datasets based on that used in:
    #https://github.com/richardaecn/class-balanced-loss
    #https://github.com/kaidic/LDAM-DRW/tree/master
    numEpochs=200
    optimizer = torch.optim.SGD(model.parameters(), lr=0.1, momentum=0.9, weight_decay=2e-4)
    scheduler = torch.optim.lr_scheduler.MultiStepLR(optimizer, [160,180], gamma=0.01)
    #if lossFunc.startswith('LMLoss'):
    #    optimizer = torch.optim.SGD(model.parameters(), lr=0.1, momentum=0.9, weight_decay=5e-4)
    #    scheduler = torch.optim.lr_scheduler.MultiStepLR(optimizer, [int(0.9*numEpochs),int(0.95*numEpochs)])
elif task.startswith('CIFAR'):
    #training set-up used for learning with AT based on that used in:
    #https://github.com/P2333/Bag-of-Tricks-for-AT
    numEpochs=110 
    optimizer = torch.optim.SGD(model.parameters(), lr=0.1, momentum=0.9, weight_decay=5e-4)
    scheduler = torch.optim.lr_scheduler.MultiStepLR(optimizer, [int(0.9*numEpochs),int(0.95*numEpochs)])
elif task.startswith('TinyImageNet'):
    numEpochs=110 
    optimizer = torch.optim.SGD(model.parameters(), lr=0.1, momentum=0.9, weight_decay=5e-4)
    scheduler = torch.optim.lr_scheduler.MultiStepLR(optimizer, [int(0.4*numEpochs),int(0.8*numEpochs)])
else:
    numEpochs=20
    optimizer = torch.optim.Adam(model.parameters(), lr=0.001)
    scheduler = torch.optim.lr_scheduler.MultiStepLR(optimizer, [int(2*numEpochs)])
compCost*numEpochs

from loss_functions import classification_loss_func

#############################################################################
# define the data augmentation methods
#############################################################################

from augment_data import data_augmentation_hyperparameters, augment_data, mixup_data, jitter_dwt
epsilon, stepsize, steps, noiseStd, noisePatchHalfSize, noiseSparsity, proportion, pixmix_loader, mixupAlpha = data_augmentation_hyperparameters(task,dataAug,batchSize,imDims,model)
if dataOOD!='none': compCost*=2
if 'PGD' in dataAug: compCost*=0.4*steps
if 'pixmix' in dataAug: compCost*=25

#############################################################################
# train the model
#############################################################################
from utils import save_checkpoint, load_checkpoint, plot_images
# if checkpoint exists for this experiment, load and resume training
from os.path import exists
if exists(fileName):
    model, optimizer, startEpoch=load_checkpoint(fileName, model, optimizer, device)
else:
    startEpoch=0

if device.type=='cpu': compCost*=40
if intFunc=='ConvCustom': compCost*=20
if compCost>1.0: print('saving checkpoints')

model.train() # prepare model for training
print('Training on',device,flush=True)
if torch.cuda.is_available():
    print(torch.cuda.get_device_name(0),flush=True)
startTime = time.time()
for epoch in range(startEpoch, numEpochs):
    trainLoss = 0.0 #initialise variable used to monitor training loss
    for samples, labels in train_loader:
        samples, labels = samples.to(device), labels.to(device)
        # clear gradients ready for new update
        optimizer.zero_grad()

        if dataAug.startswith('regmixup'):
            # forward pass: present clean inputs and compute predicted outputs
            yClean = model(samples)

        #modify training data, if requred
        if 'mixup' in dataAug:
            samples, mixupLabels, mixupProportion = mixup_data(samples, labels, mixupAlpha)
        else:
            samples=augment_data(samples, labels, dataAug, model, epsilon, stepsize, steps, noiseStd, noisePatchHalfSize, noiseSparsity, proportion, pixmix_loader)
        #plot_images(samples, labels)

        # forward pass: present inputs and compute predicted outputs
        y = model(samples)

        # calculate the classification loss
        if 'mixup' in dataAug:
            loss = mixupProportion * classification_loss_func(y, labels, lossFunc, numClasses, device)
            loss += (1.0 - mixupProportion) * classification_loss_func(y, mixupLabels, lossFunc, numClasses, device)
        else:
            loss = classification_loss_func(y, labels, lossFunc, numClasses, device)
        if 'regmixup' in dataAug:
            loss += classification_loss_func(yClean, labels, lossFunc, numClasses, device)

        # backward pass: compute gradient of the loss with respect to model parameters
        loss.backward()
        optimizer.step()
        # record training loss
        trainLoss += loss.item()*samples.size(0)
    scheduler.step()

    # calculate average loss over the epoch
    trainLoss /= len(train_loader.dataset)
    # print information about training progress
    print('End of epoch {} (lr={:.6f}) Loss={:.4f}'.format(epoch, optimizer.param_groups[0]['lr'], trainLoss),flush=True)
    # if requested perform more tests on network performance
    if intermittentAssessment and epoch%10==0:
        model.eval()
        test_network_accuracy(numClasses, test_loader, model, detailed=False)
        torch.save(model.state_dict(), fileName)
        os.system("python test_model_ood_detection.py")
        model.train()
        model.training=True
    # for methods that are slow to train, save a checkpoint to allow training to be resumed
    if compCost>1.0:
        save_checkpoint(fileName, model, optimizer, epoch+1)
    #abort learning if loss or network output contains nans
    if y.isnan().any():
        sys.exit("aborted learning due to network returning nan activation values")
    if loss.isnan():
        sys.exit("aborted learning due to loss being nan")

timeElapsed = time.time() - startTime
print('Training time {:.2f}s\n'.format(timeElapsed))

#############################################################################
# evaluate the trained model
#############################################################################

model.eval() # prepare model for testing
# save the model: note saved model not a checkpoint, so can not resume training using this file.
# also note final saved network will over-write any previously saved checkpoint
torch.save(model.state_dict(), fileName)

model.eval() # prepare model for testing
test_network_accuracy(numClasses, test_loader, model)

if netType.startswith('ConvNet'):
    for idx in range(np.size(model.numMasks)):
        tmp=model.intFunc[idx]
        if intFunc=='Conv' or intFunc=='ConvCustom':
            plot_conv_weights(tmp.weight,tmp.bias)
        elif intFunc=='ConvCentSurr':
            plot_conv_weights(tmp.conv.weight,tmp.conv.bias)
            plot_conv_weights(tmp.lat.weight)
        elif intFunc=='ConvSelect':
            plot_conv_weights(tmp.conv.weight,tmp.conv.bias)
            plot_conv_weights(tmp.convSel.weight)
        elif intFunc=='ConvNonNegW':
            plot_conv_weights(tmp.log_weight.exp(),tmp.bias)
        elif intFunc=='ConvGrouped':
            plot_conv_weights(tmp.conv[1].weight,tmp.conv[1].bias)
        else:
            plot_conv_weights(tmp.conv.weight,tmp.conv.bias)
