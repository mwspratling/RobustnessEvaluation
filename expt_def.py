#############################################################################
# Code that defines the experiment to be performed
#
# (c) 2023 Michael William Spratling
#############################################################################
def expt_def():
    # MODELS FROM REPOSITORIES
    
    #robustbench models robust to corruptions 
    netType='Diffenderfer2021Winning_LRR_CARD_Deck' # 'Modas2021PRIMEResNet18' # 'Kireev2021Effectiveness_RLATAugMix' # 'Hendrycks2020AugMix_ResNeXt' # 
    netType='RBcorruptions-'+netType #append string so model is loaded from correct repository

    #robustbench models robust to Linf adversarials 
    #netType='Wang2023Better_WRN-70-16' # 'Cui2023Decoupled_WRN-28-10' # 'Rebuffi2021Fixing_70_16_cutmix_extra' # 'Gowal2021Improving_70_16_ddpm_100m' # 'Gowal2020Uncovering_70_16_extra'
    #netType='RBLinf-'+netType #append string so model is loaded from correct repository

    #robustbench models robust to L2 adversarials 
    #netType='Wang2023Better_WRN-70-16' # 'Rebuffi2021Fixing_70_16_cutmix_extra' # 'Gowal2020Uncovering_extra' # 'Augustin2020Adversarial_34_10_extra'
    #netType='RBL2-'+netType #append string so model is loaded from correct repository

    # MODELS TRAINED BY THIS CODE
    
    #netType='WideResNet34-20' # 'ResNet18' # 'ConvNet5-3-32' # 'MLP200-200-200' # 'SResNet32' # 'PreActResNet18' # 
    task='CIFAR10' # 'MNIST' # 'CIFAR100' # 'TinyImageNet' # 
    dataOOD='none' # 
    dataAug='none' # 'regmixup' # 'pixmix' # 'PGDLinf' # 'noise' # 'PGDL2' # FGSM' #
    actFunc='ReLU' # 
    intFunc='Conv' # 
    normFunc='BatchNorm' # 'IdentityNorm' # 
    poolFunc='AvgPool' # 'MaxPool' # 
    lossFunc='CELoss' # 
    balanceMethod='None' # 
    fileName=netType+'_'+task+'_'+dataOOD+'_'+dataAug+'_'+actFunc+'_'+intFunc+'_'+normFunc+'_'+poolFunc+'_'+lossFunc+'_'+balanceMethod+'.pt'
    print(fileName)
    return netType, task, dataOOD, dataAug, actFunc, intFunc, normFunc, poolFunc, lossFunc, balanceMethod, fileName
