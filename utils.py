#############################################################################
# Code for various generally useful functions
#
# (c) 2023 Michael William Spratling
#############################################################################

import numpy as np
from pathlib import Path
import torch
import torch.nn as nn
import matplotlib.pyplot as plt
#plt.rcParams.update({'font.size': 7})
plt.style.use('seaborn-v0_8-talk')

def calc_CNN_layer_sizes(imDims,numMasks,maskSize,padding,stride,poolSize):
    mapDims=[]
    newDims=imDims[1:]
    for i in range(np.size(numMasks)):
        newDims=1+(newDims-maskSize[i]+2*padding[i])/stride[i] #W & H of conv layer
        newDims=newDims/poolSize[i] #W & H of pooling layer (assumes no padding or dilation, and stride equal to pooling area)
        mapDims.append(np.array(newDims))
    mapDims= np.asarray(mapDims) #convert a list of 1D lists into a 2D array
    print(mapDims)
    return mapDims

def print_model_details(model):
    print(model)
    #params=list(model.parameters())
    #for layer in range(len(params)):
    #    print(params[layer].size()) 
    print('Total Parameters =',sum(p.numel() for p in model.parameters()))
    params=sum(p.numel() for p in model.parameters() if p.requires_grad)
    print('Total Trainable Parameters =',params)
    print('\n',flush=True)
    return params
    
def test_network_accuracy(numClasses, test_loader, model, detailed=True):
    #test and report the classification accuracy of a network
    device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")
    model.to(device)

    # initialize variables to record metrics
    classCorrect = list(0. for i in range(numClasses))
    classTotal = list(0. for i in range(numClasses))
    for samples, labels in test_loader:
        samples, labels = samples.to(device), labels.to(device)
        # forward pass: compute predicted outputs by passing inputs to the model
        y = model(samples).detach()
        # convert outputs to predicted class
        _, pred = torch.max(y, 1)

        # compare predictions to true label
        correct = np.squeeze(pred.eq(labels.data.view_as(pred)))
        # calculate test accuracy for each class
        for i in range(samples.size(0)):
            label = labels.data[i]
            classCorrect[label] += correct[i].item()
            classTotal[label] += 1

    if detailed:
        # calculate and print test accuracies for individual classes
        for i in range(numClasses):
            if classTotal[i] > 0:
                print('Accuracy for class %d: %2d%% (%2d/%2d)' % (
                    i, 100 * classCorrect[i] / classTotal[i],
                    np.sum(classCorrect[i]), np.sum(classTotal[i])))
            else:
                print('Accuracy for class %d: N/A (no training examples)' % i)
    
    accuracyOverall=100. * np.sum(classCorrect) / np.sum(classTotal)
    print('Accuracy (for all classes): %.2f %% (%2d/%2d)\n' % (
        accuracyOverall, np.sum(classCorrect), np.sum(classTotal)),flush=True)
    return accuracyOverall

def quantise(x, levels=10, slope=10.0):
    #y=torch.round(x*levels)*(1.0/levels)
    y=torch.zeros(x.size()).to(x.device)
    for i in range(levels):
        y+=torch.sigmoid(slope*(x-(i+0.5)/(levels))) 
    y/=levels
    return y

def population_code(x, levels=10, slope=200.0):
    #expands number of channels (dim 1) by population coding the input, x. Assumes values of x are in range [0,1] 
    y=torch.tensor([]).to(x.device)
    for i in range(levels):
        vals=torch.exp(-0.5*slope*((x-(i+0.5)/(levels))**2))
        #vals=torch.sigmoid(slope*(x-(i+0.5)/(levels)))
        y=torch.cat((y,vals),dim=1)
    return y


import shutil
from os.path import exists
def save_checkpoint(fileName, model, optimizer, epoch):
    #first make copy of existing checkpoint, in case job times-out during save
    if exists(fileName):
        backupFileName=fileName+'_bckup'
        shutil.copy(fileName,backupFileName)
    #create checkpoint data structure and save it
    state={
        'model_dict': model.state_dict(),
        'optim_dict': optimizer.state_dict(),
        'epoch': epoch
    }
    torch.save(state, fileName)

def load_checkpoint(fileName, model, optimizer, device):
    print("Loading checkpoint. If this generates an error, it is because file is not a checkpoint: delete %s and re-run train_model.py to train a new model from scratch." % fileName)
    state = torch.load(fileName,map_location=device)
    epoch = state['epoch']
    model.load_state_dict(state['model_dict'])
    model.to(device) #should cause optimizer state to be cast to correct device in next line of code
    optimizer.load_state_dict(state['optim_dict'])
    print('RESUMING TRAINING from epoch',epoch,flush=True)
    return model, optimizer, epoch

def parallel_model(model, state, device):    
    for key in state.keys():
        break
    if key.startswith('module'):
        model=nn.DataParallel(model).to(device)
    return model

def import_weights(fileName, model, device):
    if Path(fileName).is_file():
        #import just the parameters from a saved checkpoint/state_dict
        state = torch.load(fileName,map_location=device)
        if 'state_dict' in state.keys():
            state=state['state_dict']
        model = parallel_model(model, state, device)   
        model.load_state_dict(state)
        model.inputNorm.inputMean=nn.Parameter(state['inputNorm.inputMean'], requires_grad=False)
        model.inputNorm.inputStd=nn.Parameter(state['inputNorm.inputStd'], requires_grad=False)
        model.to(device) 
        print('Imported weights from',fileName)
        if 'epoch' in state.keys():
            print('saved at epoch',state['epoch'],flush=True)
    else:
        print('***WARNING***: Tried to import weights from non-existent file.')
    #print_model_details(model)
    return model
    
def pasteable_array(vals):
    #print values on screen with tab seperators, so that they can be copy-and-pasted into a spreadsheet
    pasteable='\t'.join([str(v) for v in vals.numpy()])
    print(pasteable)  

def print_stats(vals,name=''):
    print(name,vals.min(),vals.max(),vals.mean(),vals.numel())

def plot_image(I, incTicks=False, colourbar=True):
    I=I.to(torch.device("cpu"))
    if I.size(0)==1:
        plt.imshow(I[0], cmap='gray_r', interpolation='none')
    elif I.dim()==2:
        plt.imshow(I, cmap='gray_r', interpolation='none')
    else:
        I=np.transpose(I, [1,2,0])
        plt.imshow(I) #, cmap='RGB')
        
    if not incTicks:
        plt.xticks([])
        plt.yticks([])
    if colourbar: plt.colorbar()
 
def plot_images(samples, labels=None, numToPlot=12):
    numToPlot=np.minimum(numToPlot,samples.size(0))
    numRows=int(np.sqrt(numToPlot))
    numCols=np.maximum(1,int(numToPlot/numRows))
    fig = plt.figure(figsize=(numCols, numRows))
    for idx in range(numToPlot):
        plt.subplot(numRows, numCols, idx+1)
        #plt.tight_layout()
        if idx==numCols*(numRows-1):
            #to avoid over-crowding, put ticks only on the bottom-left subfigure
            incTicks=True
        else:
            incTicks=False
        plot_image(samples[idx], incTicks)
        if labels!=None:
            plt.title("class {}".format(labels[idx]))
    fig
  
def plot_weights(W, imDims, numToPlot=20):
    W=W.detach().to(torch.device("cpu"))
    numToPlot=np.minimum(numToPlot,W.size(0))
    numRows=2
    numCols=np.maximum(1,int(numToPlot/numRows))
    fig = plt.figure(figsize=(numCols, numRows))
    for idx in np.arange(numToPlot):
        fig.add_subplot(numRows, numCols, idx+1)
        w=W[idx,:]
        if imDims[0]==1:
            w=w.reshape(imDims[1],imDims[2])
        else:
            w=w.reshape(imDims[0],imDims[1],imDims[2])
        plot_image(w)

def plot_conv_weights(kernels, bias=None, numToPlot=20):
    kernels=kernels.detach().to(torch.device("cpu"))
    numToPlot=np.minimum(numToPlot,kernels.size(0))
    if np.prod(kernels.size()[2:])==1:
        #plot 1x1 weights
        channels=np.minimum(32,kernels.size(1))
        plt.figure()
        for idx in np.arange(numToPlot):
            plt.plot(np.arange(channels),kernels[idx,0:channels,0,0],label = str(idx))
        plt.legend()    
    else:
        channel=0 #plot weights of 1st channel only
        #channel=kernels.size(1)-1 #plot weights in last channel only
        numRows=4
        numCols=int(np.maximum(1,np.ceil(numToPlot/numRows)))
        fig = plt.figure(figsize=(numCols, numRows))
        for idx in np.arange(numToPlot):
            fig.add_subplot(numRows, numCols, idx+1)
            plot_image(kernels[idx,channel].squeeze())
            if bias==None:
                plt.title(str(idx))
            else:
                plt.title('{:.2f}'.format(bias[idx].item()))
                
def plot_adversarials(images, clippedAdvs, success, labels, predictedLabelsClean, predictedLabelsAdv):
    #find the indeces of those samples that were initially correctly classified, but are incorrectly labelled after the attack
    idxSuccess=np.nonzero((torch.logical_and(success==True, predictedLabelsClean==labels)))
    print("  number of images whose predicted label is changed after adversarial attack = {}".format(idxSuccess.numel()))
    toPlot=min(24,idxSuccess.numel())
    #plot adversarial images
    fig = plt.figure(figsize=(6,4))
    for idx in np.arange(toPlot):
       fig.add_subplot(4, 6, idx+1)
       plot_image(clippedAdvs[int(idxSuccess[idx]),:,:,:], 0)
       plt.title("{}".format(labels[int(idxSuccess[idx])]), pad=-10, loc='left')
       plt.title("-->",pad=-10)
       plt.title("{}".format(predictedLabelsAdv[int(idxSuccess[idx])]), pad=-10, loc='right')
    #plot corresponding clean images, for comparison
    fig = plt.figure(figsize=(6,4))
    for idx in np.arange(toPlot):
       fig.add_subplot(4, 6, idx+1)
       plot_image(images[int(idxSuccess[idx]),:,:,:], 0)
       plt.title("{}".format(labels[int(idxSuccess[idx])]), pad=-10, loc='left')
       plt.title("clean", pad=-10, loc='right')
      
  
def parse_string(string,base,divider='-',limit=None):
    #If divider='-', then function takes
    #a string of the form 'baseXX' and returns the list ['XX'], or for 
    #a string of the form 'baseXX-YY-ZZ' returns a list ['XX', 'YY', 'ZZ']
    #ignores anything after the limit
    if limit!=None:
        string=string.split(limit)[0]
    if len(base)>0:
        string=string.split(base)
        if np.size(string)<2:
            print('parse_string was applied to',string,'but did not find the initial string',base)
            string=string[0]
        else:
            string=string[1]
    params=string.split(divider)
    
    return params

def sign_preserving_power(x, exponent=2.0):
    y=torch.sign(x)*torch.pow(torch.abs(x),exponent)
    return y

def delete_elements(tensor, ind):
    if torch.is_tensor(tensor):
        mask=torch.ones(tensor.size(0), dtype=torch.bool)
        mask[ind]=False
        tensor=tensor[mask]
    elif type(tensor) is list:
        ind=list(ind)
        for idx in sorted(ind, reverse = True): del tensor[idx]
    else:
        mask=np.ones(np.size(tensor,0), dtype=bool)
        ind=np.array(ind)
        mask[ind]=False
        tensor=tensor[mask]
    return tensor
