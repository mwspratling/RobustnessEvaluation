#############################################################################
# Code defining activation functions
#
# (c) 2023 Michael William Spratling
#############################################################################
import torch
import torch.nn as nn
import torch.nn.functional as F

def define_activation_function(actFunc, numChannels=None):
    #convert string defining name of function to a definition for a neural net module
    if actFunc=='ReLU':
        actFunc=nn.ReLU()
    elif actFunc=='ReLU6':
        actFunc=nn.ReLU6()
    elif actFunc=='LReLU':
        actFunc=nn.LeakyReLU()
    elif actFunc=='PReLU':
        actFunc=nn.PReLU()
    elif actFunc=='ELU':
        actFunc=nn.ELU()
    elif actFunc=='SiLU':
        actFunc=nn.SiLU()
    elif actFunc=='SELU':
        actFunc=nn.SELU()
    elif actFunc=='GELU':
        actFunc=nn.GELU()
    elif actFunc=='Softplus':
        actFunc=nn.Softplus()
    elif actFunc=='tanh':
        actFunc=nn.Tanh()
    elif actFunc=='Mish':
        actFunc=nn.Mish()
    elif actFunc=='sigmoid':
        actFunc=nn.Sigmoid()
    elif actFunc=='Identity':
        actFunc=nn.Identity()
    else:
        print("ERROR: undefined activation function")
    return actFunc

