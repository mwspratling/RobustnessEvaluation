----------------------------------------------------------------------------
# INTRODUCTION
----------------------------------------------------------------------------

This code implements the methods described in:

[M. W. Spratling, A comprehensive assessment benchmark for rigorously evaluating deep learning image classifiers.](https://arxiv.org/abs/2308.04137)

Please cite this paper if this code is used in, or to motivate, any publications. 

----------------------------------------------------------------------------
# INSTALLATION
----------------------------------------------------------------------------

This code was tested with python 3.11.3 and pytorch 2.0.1 under Ubuntu Linux version 20.04.6. To set up an appropriate python environment using anaconda execute the following at the command line (you may need to modify the yml file to install the cuda or cpu version of pytorch depending on your hardware setup):
```
conda env create --file robustness_evaluation.yml
conda activate robustness_evaluation
```

The code makes use of a number of datasets (listed below). You should modify the line:
```
dataDir='../../Data' 
```
in the file load_dataset.py to identify the directory under which you want/have this data stored.

The following datasets (if they do not already exist) will be downloaded automatically (thanks to torchvision):

+ MNIST 
+ CIFAR10 
+ CIFAR100 
+ Omniglot
+ FashionMNIST
+ KMNIST
+ SVHN
+ iNaturalist

The following dataset (if it does not already exist) will be downloaded automatically (thanks to [torchvision-tinyimagenet](https://pypi.org/project/tinyimagenet/)):

+ TinyImageNet

The remaining datasets need to be manually download from the following locations:

+ [Textures](https://www.robots.ox.ac.uk/~vgg/data/dtd/)
+ [ImageNetO](https://github.com/hendrycks/natural-adv-examples)
+ [MNISTC](https://github.com/google-research/mnist-c)
+ [CIFAR10C](https://zenodo.org/record/2535967)
+ [CIFAR100C](https://zenodo.org/record/3555552)
+ [TinyImageNetC](https://github.com/hendrycks/robustness)
+ [fractals and feature visualizations](https://drive.google.com/file/d/1qC2gIUx9ARU7zhgI4IwGD3YcFhm8J4cA/view?usp=sharing)

----------------------------------------------------------------------------
# USAGE
----------------------------------------------------------------------------

The file "expt_def.py" defines the experiment to be performed, and it should be edited accordingly.

To train a network from scratch, edit the file "expt_def.py" in order to define the network architecture, the dataset, the training data augmentation method to be used, the activation function, the normalisation method, and the loss function, for the experiment you wish to perform. Then execute the following at the command line:
```
python train_model.py
```

To test the network you have trained execute the following at the command line (without modifying expt_def.py):
```
python test_model_ood_detection.py
python test_model_all_data.py
python eval_robustness.py
```

To test a pretrained network, such as one from robustbench, define the name of the network as shown in the examples at the start of the expt_def.py file, and edit out the later re-definition of netType. Also define the dataset used to train the pretrained network. Other variables defined in expt_def.py (to define the the training data augmentation method to be used, the activation function, the normalisation method, and the loss function, etc.) will be ignored. Then run the following at the command line:
```
python test_model_ood_detection.py
python test_model_all_data.py
python eval_robustness.py
```
