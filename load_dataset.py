#############################################################################
# Code to load datasets
#
# (c) 2023 Michael William Spratling
#############################################################################

import torch
import torchvision
import numpy as np
from torchvision import datasets
from torch.utils.data import Subset
from PIL import Image
from utils import delete_elements, parse_string
import PIL.ImageOps

class invert_image(object):
    #Convert image to its negative
    def __init__(self):
        pass
    def __call__(self, img):
        return PIL.ImageOps.invert(img)

def imbalance_data(dataSet, numClasses, imbalanceFactor):
    #removes samples from a dataset to make the number of samples in each class imblanced
    nNewAll=torch.zeros(numClasses)
    
    #establish name of structure element containing class information
    labelsName=''
    try:
        dataSet.targets
        labelsName='targets'
    except:
        try:
            dataSet.target
            labelsName='target'
        except:
            try:
                dataSet.labels
                labelsName='labels'
            except:
                print('WARNING: function imbalance_dataclass failed to find class labels')
                return dataSet, nNewAll

    if dataSet!=None:
        print('Checking/creating class imbalance')
        for c in torch.arange(numClasses):
            # get indeces of samples in this class
            if labelsName=='targets':
                ind=torch.where(torch.as_tensor(dataSet.targets)==c)[0]
            elif labelsName=='target':
                ind=torch.where(torch.as_tensor(dataSet.target)==c)[0]
            elif labelsName=='labels':
                ind=torch.where(torch.as_tensor(dataSet.labels)==c)[0] 
            
            n=len(ind)                          #original number of samples for this class
            nNew=int(n*imbalanceFactor**c)      #required downsampling of this class
            nNewAll[c]=nNew

            if imbalanceFactor<1:
                indDelete=ind[nNew:]            #select indeces in excess of required number
                #remove unrequired data:
                if labelsName=='targets':
                    dataSet.targets=delete_elements(dataSet.targets,indDelete)
                elif labelsName=='target':
                    dataSet.targets=delete_elements(dataSet.target,indDelete)
                elif labelsName=='labels':
                    dataSet.targets=delete_elements(dataSet.labels,indDelete)
                dataSet.data=delete_elements(dataSet.data,indDelete)
                print('   Number of samples in class',int(c),'changed from',n,'to',nNew)
        print('   imbalanced factor (beta = n_max / n_min)=',nNewAll.max()/nNewAll.min())
    return dataSet, nNewAll
        
                    
def load_dataset(task,batchSize,augmentTrain=False,plot=False,batchSizeTest=None,corruption=None,imDimsReqd=None,shuffle=True,imbalanceFactor=1):
    dataDir='../../Data' # needs to be changed to point to correct directory where all data is stored
    if batchSizeTest==None:
        batchSizeTest=batchSize
    task=parse_string(task,'')[0] #remove any part of task name after '-'

    # define transformations and convert data to torch.FloatTensor
    if augmentTrain:
        # define transformation to augment dataset
        if task in ['MNIST','FashionMNIST']:
            transformTrain = torchvision.transforms.Compose([
                #torchvision.transforms.RandomInvert(p=0.5),
                torchvision.transforms.RandomAffine(degrees=10,translate=(0.15,0.15),scale=(0.8,1.2),shear=(5,5,5,5)),
                #torchvision.transforms.RandomCrop(28,padding=1,padding_mode="edge"),
                torchvision.transforms.ToTensor()])
        elif task.startswith('CIFAR'):
            transformTrain = torchvision.transforms.Compose([
                #torchvision.transforms.ColorJitter(0.1,0.1,0.1,0.1),
                #torchvision.transforms.RandomAffine(degrees=10,translate=(0.05,0.05),scale=(0.9,1.1),shear=(5,5,5,5)),
                torchvision.transforms.RandomCrop(32,padding=4,padding_mode="reflect"),
                torchvision.transforms.RandomHorizontalFlip(),
                torchvision.transforms.ToTensor()])
        elif task=='TinyImageNet':
            transformTrain = torchvision.transforms.Compose([
                #torchvision.transforms.ColorJitter(0.1,0.1,0.1,0.1),
                #torchvision.transforms.RandomAffine(degrees=10,translate=(0.05,0.05),scale=(0.9,1.1),shear=(5,5,5,5)),
                torchvision.transforms.RandomCrop(64,padding=4,padding_mode="reflect"),
                torchvision.transforms.RandomHorizontalFlip(),
                torchvision.transforms.ToTensor()])
        elif task=='ImageNet':
            transformTrain = torchvision.transforms.Compose([
                #torchvision.transforms.ColorJitter(0.1,0.1,0.1,0.1),
                #torchvision.transforms.RandomAffine(degrees=10,translate=(0.05,0.05),scale=(0.9,1.1),shear=(5,5,5,5)),
                torchvision.transforms.RandomCrop(224,padding=4,padding_mode="reflect"),
                torchvision.transforms.RandomHorizontalFlip(),
                torchvision.transforms.ToTensor()])
    else:
        transformTrain=torchvision.transforms.ToTensor()
    transformTest = torchvision.transforms.ToTensor()

    if imDimsReqd != None:
        #add transform to resize images to specified dimensions
        transformTrain = torchvision.transforms.Compose([
                                torchvision.transforms.Resize(imDimsReqd[1:]),
                                transformTrain])
        transformTest = torchvision.transforms.Compose([
                                torchvision.transforms.Resize(imDimsReqd[1:]),
                                transformTest])
        #convert to greyscale if necessary
        if imDimsReqd[0]==1:
            print('converting', task, 'to greyscale')
            transformTest = torchvision.transforms.Compose([
                                torchvision.transforms.Grayscale(),
                                transformTest])

    # choose the training and test datasets
    if task=='MNIST':
        trainData = torchvision.datasets.MNIST(root=dataDir, train=True, download=True, transform=transformTrain)
        testData = torchvision.datasets.MNIST(root=dataDir, train=False, download=True, transform=transformTest)
        classNames = ('0', '1', '2', '3', '4', '5', '6', '7', '8', '9')
        numClasses=10
        
    elif task=='FashionMNIST':
        trainData = torchvision.datasets.FashionMNIST(root=dataDir, train=True,
                                                download=True, transform=transformTrain)
        testData = torchvision.datasets.FashionMNIST(root=dataDir, train=False,
                                               download=True, transform=transformTest)
        classNames = trainData.classes
        numClasses=10
        
    elif task=='KMNIST':
        trainData = torchvision.datasets.KMNIST(root=dataDir, train=True,
                                                download=True, transform=transformTrain)
        testData = torchvision.datasets.KMNIST(root=dataDir, train=False,
                                               download=True, transform=transformTest)
        classNames = trainData.classes
        numClasses=len(classNames)
        
    elif task.startswith('EMNIST'):
        split=task.split('EMNIST')
        split=split[1] #can be: byclass, bymerge, balanced, letters, digits or mnist 
        trainData = torchvision.datasets.EMNIST(root=dataDir, split=split, train=True,
                                                download=True, transform=transformTrain)
        testData = torchvision.datasets.EMNIST(root=dataDir, split=split, train=False,
                                               download=True, transform=transformTest)
        classNames = trainData.classes
        numClasses=len(classNames)
        
    elif task=='CIFAR10':
        trainData = torchvision.datasets.CIFAR10(root=dataDir, train=True,
                                                download=True, transform=transformTrain)
        testData = torchvision.datasets.CIFAR10(root=dataDir, train=False,
                                               download=True, transform=transformTest)
        classNames = ('plane', 'car', 'bird', 'cat', 'deer', 'dog', 'frog', 'horse', 'ship', 'truck')
        numClasses=10
        
    elif task=='CIFAR100':
        trainData = torchvision.datasets.CIFAR100(root=dataDir, train=True,
                                                download=True, transform=transformTrain)
        testData = torchvision.datasets.CIFAR100(root=dataDir, train=False,
                                               download=True, transform=transformTest)
        classNames = trainData.classes
        numClasses=100
        
    elif task=='SVHN':
        trainData = torchvision.datasets.SVHN(root=dataDir+'/SVHN', split='train',
                                                download=True, transform=transformTrain)
        testData = torchvision.datasets.SVHN(root=dataDir+'/SVHN', split='test',
                                               download=True, transform=transformTest)
        classNames = ('0', '1', '2', '3', '4', '5', '6', '7', '8', '9')
        numClasses=10
        
    elif task=='LSUN':
        trainData = torchvision.datasets.LSUN(root=dataDir+'/LSUN', classes='train',
                                                transform=transformTrain)
        testData = torchvision.datasets.LSUN(root=dataDir+'/LSUN', classes='test',
                                               transform=transformTest)
        classNames = trainData.classes
        numClasses=100
        
    elif task=='Places365':
        trainData = torchvision.datasets.Places365(root=dataDir+'/Places365', split='train-standard',
                                                download=True, small=True, transform=transformTrain)
        testData = torchvision.datasets.Places365(root=dataDir+'/Places365', split='val',
                                               download=True, small=True, transform=transformTest)
        classNames = trainData.classes
        numClasses=100

    elif task=='iNaturalist':
        trainData = torchvision.datasets.INaturalist(root=dataDir+'/iNaturalist/',version='2021_train',
                                                    download=True, transform=transformTest)
        testData = torchvision.datasets.INaturalist(root=dataDir+'/iNaturalist/',version='2021_valid',
                                                    download=True, transform=transformTest)
        classNames = None
        numClasses=5089 # can change number of classes using target_type parameter
        
    elif task=='TinyImageNet':
        from tinyimagenet import TinyImageNet
        from pathlib import Path
        trainData = TinyImageNet(Path(dataDir),split='train', transform=transformTrain)
        testData = TinyImageNet(Path(dataDir),split='val', transform=transformTest)
        classNames = None
        numClasses=200
        
    elif task=='ImageNetO':
        #transform so that all image are the same size 
        transformTest = torchvision.transforms.Compose([
                                torchvision.transforms.CenterCrop(224),
                                transformTest])
        trainData = None
        testData = torchvision.datasets.ImageFolder(root=dataDir+'/imagenet-o/', transform=transformTest)
        classNames=None
        numClasses=200

    elif task=='ImageNet':
        transform = torchvision.transforms.Compose([
                torchvision.transforms.Resize(256),
                torchvision.transforms.CenterCrop(224)])
        transformTrain = torchvision.transforms.Compose([transform,transformTrain])
        transformTest = torchvision.transforms.Compose([transform,transformTest])
        trainData = torchvision.datasets.ImageFolder(root=dataDir+'/imagenet/train/', transform=transformTest)
        testData = torchvision.datasets.ImageFolder(root=dataDir+'/imagenet/val/', transform=transformTest)
        classNames=None
        numClasses=1000

    elif task=='Omniglot':
        #transform to have same contrast polarity as MNIST
        transformTest = torchvision.transforms.Compose([
                                invert_image(),
                                transformTest])
        trainData = None
        testData = torchvision.datasets.Omniglot(root=dataDir, download=True, 
                                                 transform=transformTest)
        classNames=None
        numClasses=964

    elif task=='Bars':
        #synthetic dataset containing images with only a single horizontal or vertical bar
        if imDimsReqd is not None:
            imDims=imDimsReqd
        else:
            imDims=[1,28,28]
        imDims=np.append(1,imDims)
        trainData = torch.tensor([])
        trainLabels = torch.tensor([]).int()
        for c in torch.arange(imDims[1]):
            for x in torch.arange(imDims[2]):
                #image with horizontal bar
                img=torch.zeros(list(imDims))
                img[0,c,x,:]=1
                trainData=torch.cat((trainData,img),dim=0)
                trainLabels=torch.cat((trainLabels,torch.tensor([0])))
            for y in torch.arange(imDims[3]):
                #image with vertical bar
                img=torch.zeros(list(imDims))
                img[0,c,:,y]=1
                trainData=torch.cat((trainData,img),dim=0)
                trainLabels=torch.cat((trainLabels,torch.tensor([1])))
        trainData = torch.utils.data.TensorDataset(trainData, trainLabels)
        testData = trainData
        numClasses=2
        classNames = ('horiz', 'vert')

    elif task.startswith('Synth'):
        #synthetic images
        noiseType=task.split('Synth')[1]
        
        trainData = None
        numSamples=10000
        numClasses=10
        testLabels = torch.randint(low=0, high=numClasses, size=(numSamples,))
        classNames=None
         
        imDimsReqd=np.append(numSamples,imDimsReqd)
 
        if noiseType.startswith('Uniform'):
            testData = torch.rand(list(imDimsReqd))
        #following synth datasets adapted from https://github.com/hendrycks/outlier-exposure/
        if noiseType.startswith('Gaussian'):
            testData = torch.from_numpy(np.float32(np.clip(
                np.random.normal(size=imDimsReqd, loc=0.5, scale=0.5), 0, 1)))
        elif noiseType.startswith('Bernoulli'):
            testData = torch.from_numpy(np.float32(
                np.random.binomial(size=imDimsReqd, n=1, p=0.5)))
        elif noiseType.startswith('Blobs'):
            testData = np.float32(np.random.binomial(size=imDimsReqd, n=1, p=0.7))
            from skimage.filters import gaussian as gblur
            for i in range(numSamples):
                testData[i] = gblur(testData[i], sigma=1.5, channel_axis=None)
                testData[i][testData[i] < 0.75] = 0.0
            testData = torch.from_numpy(testData)
        testData = torch.utils.data.TensorDataset(testData, testLabels)

    elif task=='Textures':
        #adapted from https://github.com/hendrycks/outlier-exposure/
        trainData = None
        testData = torchvision.datasets.ImageFolder(root=dataDir+'/dtd/images',
                            transform=transformTest)
        numClasses=47
        classNames=None
        
    elif task=='Icons50':
        #Dataset from here: https://github.com/hendrycks/robustness
        #adapted from https://github.com/hendrycks/outlier-exposure/
        trainData = None
        testData = torchvision.datasets.ImageFolder(dataDir+'/Icons-50/',
                                                    transform=transformTest)
        filtered_imgs = []
        for img in testData.imgs:
            if 'numbers' not in img[0]:     # img[0] is image name
                filtered_imgs.append(img)
        testData.imgs = filtered_imgs

        numClasses=50
        classNames=None

    elif task=='FractalsAndFvis':
        #Dataset from here: https://github.com/andyzoujm/pixmix
        trainData = None
        #transform so that all image are the same size 
        transformTest = torchvision.transforms.Compose([
                                torchvision.transforms.Resize(256),
                                torchvision.transforms.RandomCrop(224),
                                transformTest])
        testData = torchvision.datasets.ImageFolder(dataDir+'/fractals_and_fvis/',
                                                    transform=transformTest)
        numClasses=2
        classNames=None

    elif task=='300KRandomImages':
        #Dataset from here: https://github.com/hendrycks/outlier-exposure
        trainData = np.load(dataDir+'/300KRandomImages/300K_random_images.npy')
        trainData = trainData.transpose((0, 3, 1, 2))
        trainData = torch.from_numpy(trainData)
        trainData = trainData/255
        #dummy classes
        numSamples=trainData.size(0)
        numClasses=10
        trainLabels = torch.randint(low=0, high=numClasses, size=(numSamples,))
        classNames=None
        trainData = torch.utils.data.TensorDataset(trainData, trainLabels)
        testData = None
        
    elif task=='MNISTC':
        #corruptions=corruptionsMNIST
        trainData = None
        testData = MNISTC(dataDir+'/mnist_c/', corruption, transform=transformTest)
        numClasses=10
        classNames=None
        
    elif task=='CIFAR10C':
        #corruptions=corruptionsCIFAR
        trainData = None
        testData = CIFARC(dataDir+'/CIFAR-10-C/', corruption, transform=transformTest)
        numClasses=10
        classNames=None
        
    elif task=='CIFAR100C':
        #corruptions=corruptionsCIFAR
        trainData = None
        testData = CIFARC(dataDir+'/CIFAR-100-C/', corruption, transform=transformTest)
        numClasses=100
        classNames=None
        
    elif task=='TinyImageNetC' or task=='ImageNetC':
        #corruptions=corruptionsTinyImageNet
        trainData = None
        if task=='TinyImageNetC': 
            dirName='/Tiny-ImageNet-C/'
        else:
            dirName='/ImageNet-C/'
        
        allImages=torch.tensor([])
        allLabels=torch.tensor([])
        for severity in range(1, 6): #combine data with different corruption severity
            testDataSeverity = torchvision.datasets.ImageFolder(root=dataDir+dirName + corruption + '/' + str(severity),
                                                    transform=transformTest)
            tmp_loader = torch.utils.data.DataLoader(testDataSeverity, batch_size=batchSize,
                                                   num_workers=0, shuffle=False)
            for samples, labels in tmp_loader:
                allImages=torch.cat((allImages,samples),0)
                allLabels=torch.cat((allLabels,labels))
        allLabels=allLabels.type(torch.LongTensor)
        testData=torch.utils.data.TensorDataset(allImages,allLabels)
        numClasses=200
        classNames=None
    
    else:
        print('ERROR: unknown dataset')   
    
    #create an imbalanced version of the dataset, if required, and/or return a count of the number of training samples for each class
    trainData, numSamplesPerClass = imbalance_data(trainData, numClasses, imbalanceFactor)

    # prepare data loaders for training and testing datasets
    if trainData is None:
        trainLoader = None
    else:
        trainLoader = torch.utils.data.DataLoader(trainData, batch_size=batchSize, num_workers=0, shuffle=shuffle)
    if testData is None:
        testLoader = None
    else:
        testLoader = torch.utils.data.DataLoader(testData, batch_size=batchSizeTest, num_workers=0, shuffle=shuffle)
    
    # obtain one batch of training images
    if trainData is None:
        dataiter = iter(testLoader)
    else:
        dataiter = iter(trainLoader)
    images, labels = next(dataiter)

    imDims=np.array([images.size(1),images.size(2),images.size(3)])
    print('loaded',task,'data: numClasses=',numClasses,' imDims=',imDims,'  [min,max]=[%.2f,%.2f]' % (images.min(),images.max()))

    if plot:
        from utils import plot_images
        #plot some example training images
        plot_images(images, labels)

    return trainLoader, testLoader, imDims, numClasses, classNames, numSamplesPerClass


corruptionsCIFAR = ['gaussian_noise','shot_noise','speckle_noise','impulse_noise',
                      'defocus_blur','gaussian_blur','motion_blur','zoom_blur',
                      'snow','fog','brightness','contrast','elastic_transform',
                      'pixelate','jpeg_compression','spatter','saturate','frost']
corruptionsMNIST = ['brightness','canny_edges','dotted_line','fog','glass_blur',
                    'impulse_noise','motion_blur','rotate','scale','shear',
                    'shot_noise','spatter','stripe','translate','zigzag']
corruptionsTinyImageNet = ['gaussian_noise', 'shot_noise', 'impulse_noise',
                           'defocus_blur', 'glass_blur', 'motion_blur', 'zoom_blur',
                           'snow', 'frost', 'fog', 'brightness','contrast', 
                           'elastic_transform', 'pixelate', 'jpeg_compression']
corruptionsImageNet = corruptionsTinyImageNet

class CIFARC(datasets.VisionDataset):
    def __init__(self, root :str, name :str,
                 transform=None, target_transform=None):
        assert name in corruptionsCIFAR
        super(CIFARC, self).__init__(
            root, transform=transform,
            target_transform=target_transform
        )
        data_path = root+name+'.npy'
        target_path = root+'labels.npy'
        
        self.data = np.load(data_path)
        self.targets = np.load(target_path)
        
    def __getitem__(self, index):
        img, targets = self.data[index], self.targets[index]
        img = Image.fromarray(img)

        if self.transform is not None:
            img = self.transform(img)
        if self.target_transform is not None:
            targets = self.target_transform(targets)
    
        return img, targets
    
    def __len__(self):
        return len(self.data)

def extract_subset(dataset, num_subset :int):
    indices = [i for i in range(num_subset)]
    return Subset(dataset, indices)

class MNISTC(datasets.VisionDataset):
    def __init__(self, root :str, name :str,
                 transform=None, target_transform=None):
        assert name in corruptionsMNIST
        super(MNISTC, self).__init__(
            root, transform=transform,
            target_transform=target_transform
        )
        data_path = root+name + '/test_images.npy'
        target_path = root+name + '/test_labels.npy'
        
        self.data = np.load(data_path)
        self.targets = np.load(target_path)
        
    def __getitem__(self, index):
        img, targets = self.data[index], self.targets[index]
        img=img.reshape(28,28)
        
        if self.transform is not None:
            img = self.transform(img)
        if self.target_transform is not None:
            targets = self.target_transform(targets)
   
        return img, targets
    
    def __len__(self):
        return len(self.data)

