#############################################################################
#  Code defining norm functions
#
# (c) 2023 Michael William Spratling
#############################################################################
import torch
import torch.nn as nn
import torch.nn.functional as F

def define_norm_function(normFunc, channels):
    #convert string defining name of function to a definition for a neural net module
    if normFunc=='IdentityNorm':
        normFunc=nn.Identity()
    elif normFunc=='BatchNorm':
        normFunc=nn.BatchNorm2d(channels)
    elif normFunc=='InstanceNorm':
        normFunc=nn.InstanceNorm2d(channels)
    else:
        print("ERROR: undefined norm function")
    return normFunc

