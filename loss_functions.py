#############################################################################
# Code defining methods for updating weights
# (loss functions, regularisation methods and learning rules)
#
# (c) 2023 Michael William Spratling
#############################################################################

import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
from utils import parse_string, print_stats

#############################################################################
# Loss functions
#############################################################################
def classification_loss_func(y, labels, lossFunc, numClasses, device, params=None, weightPerClass=None):
    #loss function for classification/prediction accuracy

    # convert labels to a one-hot encoding
    target = F.one_hot(labels, num_classes=numClasses).type(torch.FloatTensor)
    target = target.to(device)

    if lossFunc.startswith('CELoss'): #=Cross-Entropy Loss=NLL after log_softmax activation
        #loss = -torch.mean(torch.sum(target*F.log_softmax(y,-1),dim=-1))
        loss=F.cross_entropy(y,labels, weight=weightPerClass)
    elif lossFunc.startswith('SELoss'): #mean Squared Error Loss
        #loss=torch.mean(torch.mean((target-y)**2,dim=-1))
        loss=F.mse_loss(y,target)
    elif lossFunc.startswith('AELoss'): #=mean Absolute Error Loss
        #loss = torch.mean(torch.mean(torch.abs(target-y),dim=-1))
        loss=F.l1_loss(y,target)
    return loss
    

#############################################################################
# Regularisation methods
#############################################################################

